TCC Versao1.0
============
Inicio do projeto de TCC

Objetivos do projeto
- Sistema de monitoramento 

- Mostrar o valor total do gasto de energia (espero poder usar os valores tabelados pela aneel)
- Mostrar o valor individual por tomadas (tensão(127, 220), potencia(w) consumo(kWh/mes), temperatura(C))
- Calculo dos gastos
- Calculo de economia (Somente se tevermos a eficiencia energetica)
- Verificar se a eficiencia energetica por aparelho
- Mostrar gráfico de comparação de carga utilizada por fase (visão técnica)
- Mostrar aviso de sobrecarga de tomadas ou fases. (novo padrão 10A e 20A dependendo da bitola da tomada)(fase definida pela chave geral)

- Dashboard
	- Tela inicial do sistema aonde mostrar os graficos de:
		- Consumo total atual (em W e KWh)
		- Consumo total mensal (KWh) deve ser possível mudar o dia do fechamento pois a leitura no relógio não é sempre no mesmo dia
		- Os aparelhoes com maiores consumos (gráfico)
		- Mostrar os aparelhos com que passam da eficiencia energetica indicada pelo inmetro (se der tempo)
		

- Cadastro de aparelho
	- campos:
		- tipo do aparelho (com lista dos tipos ja cadastrados)
		- marca do aparelho (com lista das marcas ja cadastradas)
		- familia do aparelho (?)
		- modelo do aparelho (com lista dos modelos ja cadastrados)
		- categoria do aparelho (mostrar categorias do immetro?)
		- Comodo da casa (se possível disponibilizar layout da casa)
		- Numero do sensor que utilizara (nº do dipositivo e porta utilizada)

- Consultar aparelho
	- filtro:
		- por nome
		- por marca
		- por modelo
		- por categoria
		- por comodo da casa
	- O resultado deve trazer uma table com os campos
		- nome, marca, categoria, e comodo
		- uma opcao para mais detelhes, onde ao clicar sera encaminhado para ver mais detalhes do aparelho

- Análise dos gastos com energia (precisa de outro nome)
	- Calculo dos gastos por período
	- Calculo de economia de energia (Somente se tevermos a eficiencia energetica)
	- Calculo do gasto total por aparelhos selecionados

- Eficiencia energica (talvez fazer)
