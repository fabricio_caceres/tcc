'use strict';

var AparelhoController = new function () {

	this.load = function ($scope, $http, rest) {
    $scope.aparelhos = [];
    $scope.tomadas = [];
    $scope.modulos = [];
    $scope.novo_aparelho = {};

    $scope.viewAdiciona = function (){
      $("#myModal").modal('show');
      $(":input").inputmask();
    };

    $scope.listar = function (){
      rest.get('/aparelho')
      .success(function (data) {
        if(data.status=="sucesso") {
          $scope.aparelhos =  data.aparelhos;
        }
      })
      .error( function (data) {
      });
    };

  	$scope.adicionar = function (){

      //==========INICIO DTHR_INICIO =========
      var auxData = $scope.novo_aparelho.dthr_inicio;
      console.log(auxData);

      auxData = auxData.split(' ');
      var auxDataHora = auxData[1];
      auxData = auxData[0];
      auxData = auxData.split('/');
      //converte em foma de db
      auxData = auxData[2]+'-'+auxData[1]+'-'+auxData[1];

      if(auxDataHora == '__:__' || auxDataHora == "" || auxDataHora === undefined){
        auxDataHora = "00:00";
      }

      $scope.novo_aparelho.dthr_inicio = auxData+" "+auxDataHora;
      //==========FIM DTHR_INICIO =========

      //==========INCIO DTHR_FIM =========
      var auxData = $scope.novo_aparelho.dthr_fim;
      console.log(auxData);

      auxData = auxData.split(' ');
      var auxDataHora = auxData[1];
      auxData = auxData[0];
      auxData = auxData.split('/');
      //converte em foma de db
      auxData = auxData[2]+'-'+auxData[1]+'-'+auxData[1];

      if(auxDataHora == '__:__' || auxDataHora == "" || auxDataHora === undefined){
        auxDataHora = "00:00";
      }

      $scope.novo_aparelho.dthr_fim = auxData+" "+auxDataHora;
      //=========FIM DTHR_FIM =========

      rest.post('/aparelho', $scope.novo_aparelho)
      .success(function (data) {
        if(data.status=="sucesso") {
          $scope.listar();
          $("#myModal").modal('hide');
        }
        $scope.novo_aparelho = {};
      })
      .error( function (data) {
      });
    };

    $scope.getModulos = function (){
      $http.get(API.server+'/moduloTomada')
        .success(function (data) {
          if(data.status=="sucesso") {
            $scope.modulos =  data.modulos;
          }
        })
        .error(function (data) {
        });
    };

    $scope.getTomada = function () {
      angular.forEach($scope.modulos, function(value, key) {
        if(value.no_modulo == $scope.novo_aparelho.no_modulo){
          this.push(value.tomada);
        }
      }, $scope.tomadas);
    };

    $scope.listar();
    $scope.getModulos();
  };

};