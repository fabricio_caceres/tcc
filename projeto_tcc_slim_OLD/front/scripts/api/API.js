/* ========================
 * API
* @author FABRICIO
* @created 08/12/2014 09:05:02
* @version 1.0
* ========================*/

'use strict'; 


var API = new function(){
    
    // CONTRUCTOR 
    this.templatePath = 'template'; // VAR WHERE THE ARE TEMPLATES 
    this.imageUrl = 'img/'; // VAR WHERE THE ARE IMG
    this.server = 'http://localhost/projeto_tcc/projeto_tcc_slim/back';
    this.apiVersion = 'api/v1';
};