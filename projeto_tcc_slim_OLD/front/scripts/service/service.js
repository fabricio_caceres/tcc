﻿Application.factory('rest', function ($http, $q) {
    return {
        get: function (url) {
            return $http({
                method: 'GET',
                url: (API.server +  url),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
        },
        post: function (url, data) {
            return $http({
                method: 'POST',
                url: (API.server + url),
                data: $.param(data),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
        },
        put: function (url, data) {
            return $http({
                method: 'PUT',
                url: (API.server + url),
                data: $.param(data),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
        },
        'delete': function (url) {
            return $http({
                method: 'DELETE',
                url: (API.server + url),
                data: $.param(data),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
        },
        search: function (url, filter) {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: (API.server + API.apiVersion + url),
                data: $.param(filter),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data, status, headers, config) {
                deferred.resolve($http.get(API.server + headers('Location')));
            }).error(function (data, a, c) {
                    deferred.reject(data);
                });

            return deferred.promise;
        }
    };
});
