/* ========================
 * CONFIG FOR SYSTEM
* @author FABRICIO
* @created 08/12/2014
* @version 1.0
* ========================*/

'use strict';

Application.config(function ($stateProvider, $urlRouterProvider) {

    //The default route
    $urlRouterProvider.otherwise("/");

    //Routing the states of application
    $stateProvider.state('dashboard', {
        url: "/",
    	templateUrl: API.templatePath + "/dashboard.html"
    	//controller: DeviceController.load
    });
    $stateProvider.state('modulo_tomada', {
        url: "/modulo_tomada",
    	templateUrl: API.templatePath + "/modulo_tomada/listar.html",
    	controller: ModuloTomadaController.viewListar,
    });
    $stateProvider.state('tomada', {
        url: "/tomada",
    	templateUrl: API.templatePath + "/tomada/listar.html",
    	controller: TomadaController.viewListar
    });
    $stateProvider.state('aparelho', {
        url: "/aparelho",
    	templateUrl: API.templatePath + "/aparelho/listar.html",
    	controller: AparelhoController.load
    });

//    //Routing the states of application
//    $stateProvider
//    	.state('device', {
//    		url: "/cadastro/aparelho",
//    		templateUrl: API.templatePath + "/device/registration-device.html"
//    		//controller: DeviceController.load
//    	});
//
//    //Routing the states of application
//    $stateProvider
//        .state('arduino', {
//            url: "/cadastro/arduino",
//            templateUrl: API.templatePath + "/device/registration-arduino.html"
//           	//controller: RegistrationController.load
//        });
//
//	//Routing the states of application
//    $stateProvider
//        .state('list-device', {
//            url: "/listar/aparelho",
//            templateUrl: API.templatePath + "/device/list-device.html",
//           	controller: DeviceController.listDevice
//        });
//    //Routing the states of application
//    $stateProvider
//        .state('list-arduino', {
//            url: "/listar/arduino",
//            templateUrl: API.templatePath + "/device/list-arduino.html",
//           	controller: DeviceController.listArduino
//        });
});

// .state('filter', {
//             url: "/filter",
//             templateUrl: API.templatePath + "/filter.html",
//             controller: HomeController.init/*,
//             resolve:{
//                 promise: HomeController.promiseInit
//             }*/
//         });