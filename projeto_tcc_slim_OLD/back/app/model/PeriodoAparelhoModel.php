<?php
if( !defined('ABSPATH')) die("Acesso negado");

__autoload("class/InterfaceModel");
__autoload("class/DataBase");

class PeriodoAparelhoModel implements InterfaceModel {
    private $dataBase = null;

    public $id_periodo_aparelho;
    public $tomada;
    public $no_modulo;
    public $id_aparelho;
    public $dthr_inicio;
    public $dthr_fim;


    public function __construct() {
      $this->dataBase = new DataBase();
    }

  public function adicionar($data){

  }
  public function alterar($data, $id){}
  public function excluir($id){}
  public function listar($id){}
}