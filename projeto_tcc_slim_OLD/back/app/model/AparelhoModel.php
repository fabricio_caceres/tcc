<?php
if( !defined('ABSPATH')) die("Acesso negado");

__autoload("class/InterfaceModel");
__autoload("class/DataBase");
__autoload("model/PeriodoAparelhoModel");

class AparelhoModel implements InterfaceModel {
	private $dataBase = null;

	public $id_aparelho;
  public $nome_aparelho;
	public $modelo_aparelho;
  public $localizacao;
	// public $id_inmetro;
	// public $id_usuario;


	public function __construct() {
		$this->dataBase = new DataBase();
	}

	public function adicionar($data){

		if(!isset($data['nome_aparelho']) || $data['nome_aparelho'] == "" ) {
			$retorno["status"] = "erro";
			$retorno["msg"] = "Nome do aparelho é obrigatorio";

			return $retorno;
		}

		// $this->nome_aparelho	= $data["nome_aparelho"];
		// $this->no_modulo      = isset($data["no_modulo"])? $data["no_modulo"] : "" ;
		// $this->tomada 		    = isset($data["tomada"])? $data["tomada"] : "" ;
  //   $this->dthr_inicio    = isset($data["dthr_inicio"])? $data["dthr_inicio"] : "";
  //   $this->dthr_fim       = isset($data["dthr_fim"])? $data["dthr_fim"] : "";
  //   $this->localizacao    = isset($data["localizacao"])? $data["localizacao"] : "";
  //
    $this->nome_aparelho = $data["nome_aparelho"];
    $this->modelo_aparelho = $data["modelo_aparelho"];
    $this->localizacao    = isset($data["localizacao"])? $data["localizacao"] : "";

		try{
			$conn = $this->dataBase->getConexao();
			$insert_aparelho = "INSERT INTO aparelho
                (nome_aparelho, localizacao, modelo_aparelho)
                VALUES (?, ?, ?)";
			try{

				$query = $conn->prepare($insert);
				$conn->beginTransaction();
				$query->execute(
            array($this->nome_aparelho,
                  $this->localizacao,
                  $this->modelo_aparelho)
            );
				$conn->commit();
			  $data["id_tomada"] = $conn->lastInsertId("aparelho_id_aparelho_seq");
				$conn = null;

        $periodoAparelho = new PeriodoAparelhoModel();
        $retorno = $periodoAparelho->adicionar($data);

				return $retorno;

			}catch(PDOException $e) {
				$conn->rollback();
				$retorno["status"] = "erro";
				$retorno["msg"] = "Erro: ".$e->getMessage();
				echo json_encode($retorno);
				die();
			}catch(ErrorException $e1){
        $conn->rollback();
				$retorno["status"] = "erro";
				$retorno["msg"] = "Erro: ".$e1->getMessage();
				echo json_encode($retorno);
				die();
			}
		}catch(PDOException $e) {
      $conn->rollback();
			$retorno["status"] = "erro";
			$retorno["msg"] = "Erro: ".$e->getMessage();
			echo json_encode($retorno);
			die();
		}
	}

	public function alterar($data, $id){}
	public function excluir($id){}
	public function listar($id_aparelho){

    $select = "SELECT a.id_aparelho, a.nome_aparelho, a.modelo_aparelho,
                    a.localizacao, pa.*
                   FROM aparelho a, periodo_aparelho pa
                   WHERE pa.id_aparelho = a.id_aparelho";

    if(isset($id_aparelho) && $id_aparelho != 0){
      $select .= " AND pa.id_aparelho = ".$id_aparelho;
    }
    $select .=" ORDER BY a.id_aparelho ASC";

		try{
			$conn = $this->dataBase->getConexao();
			$query = $conn->prepare($select);
			$query->execute();
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query = $query->fetchAll();
			$conn = null;

      if(sizeof($query) <= 0) {
        $retorno["msg"] = "Não possui Aparelho cadastrado";
      }
			$retorno["status"] = "sucesso";
			$retorno["aparelhos"] = $query;

			return $retorno;

		}catch(PDOException $e) {
			$retorno["status"] = "erro";
			$retorno["msg"] = "Erro: ".$e->getMessage();
			echo json_encode($retorno);
			die();

		}catch(ErrorException $e1) {
			$retorno["status"] = "erro";
			$retorno["msg"] = "Erro: ".$e1->getMessage();
			echo json_encode($retorno);
			die();
		}
	}
}