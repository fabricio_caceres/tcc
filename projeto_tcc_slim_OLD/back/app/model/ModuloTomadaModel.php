<?php 
if( !defined('ABSPATH')) die("Acesso negado");

__autoload("class/DataBase");

class ModuloTomadaModel {
	private $dataBase = null;

	public $id_modulo_tomada;
	public $nor_modulo;
  public $tomada;
  public $corrente;

	public function __construct() {
		$this->dataBase = new DataBase();
	}

	public function listarModuloDisponivel(){
		try{
			$conn = $this->dataBase->getConexao();
			$query = $conn->prepare("
        SELECT mt.no_modulo, mt.tomada
        FROM modulo_tomada mt
        WHERE (mt.no_modulo not in (SELECT pa.no_modulo FROM periodo_aparelho pa)
          OR mt.tomada not in (SELECT pa.tomada FROM periodo_aparelho pa) )
          OR mt.data_hora > (SELECT pa.dthr_fim 
                              FROM periodo_aparelho pa
                              WHERE pa.no_modulo = mt.no_modulo
                              AND pa.tomada = mt.tomada)
        GROUP BY 
          mt.no_modulo, mt.tomada
        ORDER BY 
          mt.no_modulo, mt.tomada ASC");
			$query->execute();
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$query = $query->fetchAll();
			$conn = null;

			$retorno["status"] = "sucesso";
			$retorno["modulos"] = $query;

			return $retorno;

		}catch(PDOException $e) {
			$retorno["status"] = "erro";
			$retorno["msg"] = "Erro: ".$e->getMessage();
			echo json_encode($retorno);
			die();

		}catch(ErrorException $e1) {
			$retorno["status"] = "erro";
			$retorno["msg"] = "Erro: ".$e1->getMessage();
			echo json_encode($retorno);
			die();
		}
	}

  public function getDatosByIdAparelho($id_aparelho, $id_modulo_tomada){  
    if(!isset($id_aparelho) || $id_aparelho == "" ) {
      $retorno["status"] = "erro";
      $retorno["msg"] = "Id do aparelho é obrigatorio";
      return $retorno;
    } 

    $select = "
      SELECT mt.* 
      FROM modulo_tomada mt, periodo_aparelho pa
      WHERE pa.id_aparelho =".$id_aparelho."
      AND mt.no_modulo = pa.no_modulo
      AND mt.tomada = pa.tomada
      AND mt.data_hora >= pa.dthr_inicio
      AND mt.data_hora <= pa.dthr_fim";

    if(isset($id_modulo_tomada)){
      $query.=" AND mt.id_modulo_tomada > ".$id_modulo_tomada;
    }  
    $query.=" ORDER BY mt.id_periodo_aparelho";

    try{
      $conn = $this->dataBase->getConexao();
      $query = $conn->prepare($select);
      $query->execute();
      $query->setFetchMode(PDO::FETCH_ASSOC);
      $query = $query->fetchAll();
      $conn = null;

      $retorno["status"] = "sucesso";
      $retorno["modulo_tomada"] = $query;

    }catch(PDOException $e) {
      $retorno["status"] = "erro";
      $retorno["msg"] = "Erro: ".$e->getMessage();

    }catch(ErrorException $e1) {
      $retorno["status"] = "erro";
      $retorno["msg"] = "Erro: ".$e1->getMessage();
    }

    return $retorno;

  }
}