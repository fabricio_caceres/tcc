<?php 
if( !defined('ABSPATH')) die("Acesso negado");;

__autoload("class/InterfaceController");
__autoload("model/ModuloTomadaModel");

class ModuloTomadaController{
	private $app;

	public function __construct() {
		$this->app = Slim\Slim::getInstance();
	}
	
	public function listarModuloDisponivel() {

		$moduloTomada = new ModuloTomadaModel();
		$result = $moduloTomada->listarModuloDisponivel();
		$this->app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
		$this->app->response->headers->set('Access-Control-Allow-Origin', '*');
		$this->app->response->headers->set('Access-Control-Allow-Headers', '*');
		if($result["status"] == "erro"){
			$this->app->response->status(400);	
		}else if($result["status"] == "sucesso"){
			$this->app->response->status(200);
		}
		$this->app->response->body(json_encode($result));
	}

  public function getDadosByIdAparelho ($id_aparelho = 0, $id_modulo_tomada = 0){
    $moduloTomada = new ModuloTomadaModel();
    $result = $moduloTomada->getDatosByIdAparelho($id_aparelho, $id_modulo_tomada);
    $this->app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
    $this->app->response->headers->set('Access-Control-Allow-Origin', '*');
    $this->app->response->headers->set('Access-Control-Allow-Headers', '*');
    if($result["status"] == "erro"){
      $this->app->response->status(400);  
    }else if($result["status"] == "sucesso"){
      $this->app->response->status(200);
    }
    $this->app->response->body(json_encode($result));
  }
}