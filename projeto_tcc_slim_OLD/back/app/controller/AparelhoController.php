<?php 
if( !defined('ABSPATH')) die("Acesso negado");;

__autoload("class/InterfaceController");
__autoload("model/AparelhoModel");

class AparelhoController implements InterfaceController {
	private $app;

	public function __construct() {
		$this->app = Slim\Slim::getInstance();
	}
	
	public function get($id_aparelho = 0) {
		$aparelho = new AparelhoModel();
		$result = $aparelho->listar($id_aparelho);
		$this->app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
		$this->app->response->headers->set('Access-Control-Allow-Origin', '*');
		$this->app->response->headers->set('Access-Control-Allow-Headers', '*');
		if($result["status"] == "erro"){
			$this->app->response->status(400);	
		}else if($result["status"] == "sucesso"){
			$this->app->response->status(200);
		}
		$this->app->response->body(json_encode($result));
	}

/*
	public function search($id) {
		$aparelho = new AparelhoModel();
		$result = $aparelho->listarById($id);
		$this->app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
		$this->app->response->headers->set('Access-Control-Allow-Origin', '*');
		$this->app->response->headers->set('Access-Control-Allow-Headers', '*');
		if($result["status"] == "erro"){
			$this->app->response->status(400);	
		}else if($result["status"] == "sucesso"){
			$this->app->response->status(200);
		}
		$this->app->response->body(json_encode($result));
	}*/
	public function post() {
		$aparelho = new AparelhoModel();
		$result = $aparelho->adicionar($_POST);
		$this->app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
		$this->app->response->headers->set('Access-Control-Allow-Origin', '*');
		$this->app->response->headers->set('Access-Control-Allow-Headers', '*');

		if($result["status"] == "erro"){
			$this->app->response->status(400);	
		}else if($result["status"] == "sucesso"){
			$this->app->response->status(200);
		}
		$this->app->response->body(json_encode($result));

	}
	public function puts() {}
	public function delete() {}
}