<?php 
if( !defined('ABSPATH')) die("Acesso negado");;

interface InterfaceController {

	public function get($id_aparelho);
	public function post();
	public function puts();
	public function delete();
}