<?php
if( !defined('ABSPATH')) die("Acesso negado");;

__autoload("/controller/ModuloTomadaController");
__autoload("/controller/AparelhoController");

/*==== TESTECONTROLLER =====*/
// $app->get('/teste', 'TesteController:get');
// $app->post('/teste','TesteController::post');
// $app->get('/teste/:id','TesteController::get');
// $app->put('/teste/:id','TesteController::put');
// $app->delete('/teste/:id','TesteController::delete');

/*==== MODULOTOMADACONTROLLER =====*/
$app->get('/moduloTomada', 'ModuloTomadaController:listarModuloDisponivel');
$app->get('/moduloTomada/:id_aparelho(/:id_modulo_tomada)','ModuloTomadaController::getDadosByIdAparelho');
// $app->put('/teste/:id','TesteController::put');
// $app->delete('/teste/:id','TesteController::delete');

/*==== APARELHOCONTROLLER =====*/
$app->get('/aparelho(/:id_aparelho)', 'AparelhoController:get');
$app->post('/aparelho','AparelhoController:post');
// $app->put('/teste/:id','TesteController::put');
// $app->delete('/teste/:id','TesteController::delete');

