<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modulo_tomada_model extends CI_Model {
  private $id_modulo_tomada;
  private $nor_modulo;
  private $tomada;
  private $corrente;

  //Função que retorna os modulos e tomadas dipsoniveis
  public function listarModuloDisponivel(){
    $query = "SELECT mt.no_modulo, mt.tomada
        FROM modulo_tomada mt
        WHERE (mt.no_modulo not in (SELECT pa.no_modulo FROM periodo_aparelho pa)
          OR mt.tomada not in (SELECT pa.tomada FROM periodo_aparelho pa) )
          OR mt.data_hora > (SELECT pa.dthr_fim
                              FROM periodo_aparelho pa
                              WHERE pa.no_modulo = mt.no_modulo
                              AND pa.tomada = mt.tomada)
        GROUP BY
          mt.no_modulo, mt.tomada
        ORDER BY
          mt.no_modulo, mt.tomada ASC";

    $resultado = $this->db->query($query)->result();

    $retorno["status"] = "sucesso";
    $retorno["modulos"] = $resultado;
    return $retorno;
  }

  public function getDatosByIdAparelho($id_aparelho, $id_modulo_tomada){

    $select = "
      SELECT mt.*
      FROM modulo_tomada mt, periodo_aparelho pa
      WHERE pa.id_aparelho =".$id_aparelho."
      AND mt.no_modulo = pa.no_modulo
      AND mt.tomada = pa.tomada
      AND mt.data_hora >= pa.dthr_inicio
      AND mt.data_hora <= pa.dthr_fim";

    if(isset($id_modulo_tomada)){
      $query.=" AND mt.id_modulo_tomada > ".$id_modulo_tomada
    }
    $query.=" ORDER BY mt.id_periodo_aparelho";

    $resultado = $this->db->query($select);
    $retorno["status"] = "sucesso";
    $retorno["modulo_tomada"] = $resultado;
    return $resultado;
  }

}

/* End of file modulo_tomada_model.php */

