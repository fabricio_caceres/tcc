<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aparelho_model extends CI_Model {
    private $id_aparelho;
    private $nome_aparelho;
    private $modelo_aparelho;
    private $id_inmetro;
    private $id_usuario;
    private $localizacao;

    public function adicionar($data){
      $config = array(
                 array(
                       'field'   => 'nome_aparelho',
                       'label'   => 'Nome Aparelho',
                       'rules'   => 'required'
                    )
              );

      $this->form_validation->set_rules($config);

      if ($this->form_validation->run() == FALSE)
      {
        $resposta['status'] = 'erro';
        $resposta['msg'] = validation_errors();
      }
      else
      {
        $this->id_aparelho = null;
        $this->nome_aparelho  = $data["nome_aparelho"];
        $this->modelo_aparelho  = $data["modelo_aparelho"];
        $this->localizacao    = isset($data["localizacao"])? $data["localizacao"] : null;
        $this->id_inmetro = isset($data["id_inmetro"])? $data["id_inmetro"] : null;
        $this->id_usuario = isset($data["id_usuario"])? $data["id_usuario"] : null;

        $this->db->trans_begin();

        $this->db->insert('aparelho', $this);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $retorno["status"] = "erro";

            $msg = $this->db->_error_message();
            $num = $this->db->_error_number();
            $retorno['msg'] = "Error(".$num.") ".$msg;
        }
        else
        {
            $this->db->trans_commit();
            $data["id_aparelho"] = $this->db->insert_id();
            $this->load->model('periodo_aparelho_model');
            $retorno = $this->periodo_aparelho_model->adicionar($data);

        }
        return $retorno;
      }
    }

    public function listar(){
      $select = "SELECT a.id_aparelho, a.nome_aparelho, a.modelo_aparelho,
                      a.localizacao, pa.no_modulo, pa.tomada,
                      to_char(pa.dthr_inicio, 'dd/mm/YYYY HH24:MM') as dthr_inicio,
                      to_char(pa.dthr_fim, 'dd/mm/YYYY HH24:MM') as dthr_fim
                 FROM aparelho a, periodo_aparelho pa
                 WHERE pa.id_aparelho = a.id_aparelho
                 ORDER BY a.id_aparelho ASC";

      $result = $this->db->query($select)->result();

      $retorno['status'] = "sucesso";
      $retorno['aparelhos'] = $result;

      return $retorno;
    }
}

/* End of file aparelho_model.php */
/* Location: ./application/models/aparelho_model.php */