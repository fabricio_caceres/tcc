<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periodo_aparelho_model extends CI_Model {
  private $id_periodo_aparelho;
  private $tomada;
  private $no_modulo;
  private $id_aparelho;
  private $dthr_inicio;
  private $dthr_fim;

  public function adicionar($data)
  {
    $config = array(
               array(
                     'field'   => 'tomada',
                     'label'   => 'Numero Tomada',
                     'rules'   => 'required'
               ),
               array(
                     'field'   => 'no_modulo',
                     'label'   => 'Numero do Modulo',
                     'rules'   => 'required'
                ),
               array(
                     'field'   => 'dthr_inicio',
                     'label'   => 'Data Inicial',
                     'rules'   => 'required'
                ),

            );

    $this->form_validation->set_rules($config);

    if ($this->form_validation->run() == FALSE)
    {
      $resposta['status'] = 'erro';
      $resposta['msg'] = validation_errors();
    }
    else
    {
      $this->id_periodo_aparelho = null;
      $this->tomada = $data["tomada"];
      $this->no_modulo = $data["no_modulo"];
      $this->id_aparelho = $data["id_aparelho"];
      $this->dthr_inicio = $data["dthr_inicio"];
      $this->dthr_fim = $data["dthr_fim"];

      $this->db->trans_begin();

      $this->db->insert('periodo_aparelho', $this);

      if ($this->db->trans_status() === FALSE)
      {
          $this->db->trans_rollback();
          $retorno["status"] = "erro";

          $msg = $this->db->_error_message();
          $num = $this->db->_error_number();
          $retorno['msg'] = "Error(".$num.") ".$msg;
      }
      else
      {
          $this->db->trans_commit();
          $retorno["status"] = "sucesso";
      }

      return $retorno;
    }
  }

}

/* End of file periodo_aparelho_model.php */
/* Location: ./application/models/periodo_aparelho_model.php */