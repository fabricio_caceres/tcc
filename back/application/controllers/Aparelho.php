<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aparelho extends CI_Controller {

  function __construct() {
    parent::__construct();

    $this->output->set_content_type('application/json');
    $this->output->set_header('Access-Control-Allow-Origin: *');
    $this->output->set_header('Access-Control-Allow-Headers: *');
    $this->load->model('aparelho_model');
  }

  public function get()
  {
    $resultado = $this->aparelho_model->listar();

    if($resultado["status"] == "erro"){
      $this->output->set_status_header(400);
    }else if($resultado["status"] == "sucesso"){
      $this->output->set_status_header(200);
    }

    $this->output->set_output(json_encode($resultado));
  }

  public function post() {
    $resultado = $this->aparelho_model->adicionar($_POST);

    if($resultado["status"] == "erro"){
      $this->output->set_status_header(400);
    }else if($resultado["status"] == "sucesso"){
      $this->output->set_status_header(200);
    }
    $this->output->set_output(json_encode($resultado));

  }

  // public function search($id) {
  //   $this->load->model('aparelho_model');

  //   $resultado = $this->aparelho_model->listar($id);

  //   if($resultado["status"] == "erro"){
  //     $this->output->set_status_header(400);
  //   }else if($resultado["status"] == "sucesso"){
  //     $this->output->set_status_header(200);
  //   }
  //   $this->output->set_output(json_encode($resultado));
  // }
}

/* End of file aparelho.php */
/* Location: ./application/controllers/aparelho.php */