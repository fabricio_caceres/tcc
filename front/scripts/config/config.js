/* ========================
 * CONFIG FOR SYSTEM
* @author FABRICIO
* @created 08/12/2014 
* @version 1.0
* ========================*/

'use strict'

//START ANGULAR JS
var Application = angular.module('Application', ['ui.router'])
 	.run(function($rootScope) {
	
		$rootScope.$on('$stateChangeStart',
			function(event, toState, toParams, fromState, fromParams){
//				$('.preloader').show();
			});
			
		$rootScope.$on('$stateChangeSuccess',
			function(event, toState, toParams, fromState, fromParams){
//				$('.preloader').hide();
			});
});