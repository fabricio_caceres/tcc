<?php
// function get_hora_data($datas){

//   $data_anterior ='';
//   $hora = 0;
//   foreach ($datas as $data) {
//     $data_atual = DateTime::createFromFormat('d/m/Y H:i:s', $data);

//     if($data_anterior == '' || $data_atual->format('d/m/Y') != $data_anterior->format('d/m/Y')){
//       $data_anterior = $data_atual;
//       continue;
//     }else if($data_atual->format('d/m/Y') == $data_anterior->format('d/m/Y')){
//        $tempo_atual = calcula_tempo($data_anterior->format('H:i:s'), $data_atual->format('H:i:s'));
//        $hora = $hora + ($tempo_atual <= 0? 0: $tempo_atual);
//        $data_anterior = $data_atual;
//     }
//   }

//   return $hora;
// }

// function calcula_tempo($inicial, $fim){
//   $index = 1;
//   $tempo_total;

//   $tempos = array($inicial, $fim);
//   foreach ($tempos as $tempo) {
//     $segundos = 0;

//     list($h, $m, $s) = explode(':', $tempo);
//     $segundos += $h * 3600;
//     $segundos += $m * 60;
//     $segundos += $s;

//     $tempo_total[$index] = $segundos;
//     $index++;
//   }
//   $segundos = $tempo_total[2] - $tempo_total[1];
//   $horas = $segundos/3600;

//   return $horas;

// }

function get_hora_data($datas, $hora_uso){
  $data_inicial =  DateTime::createFromFormat('d/m/Y H:i:s', $datas[0]);
  $data_final = DateTime::createFromFormat('d/m/Y H:i:s', $datas[sizeof($datas)-1]);
  $diff = date_diff($data_inicial, $data_final);
  $horas = ($diff->format('%a')+1)*intval($hora_uso);
  // var_dump($datas);
  // var_dump($horas);
  return $horas;
}
