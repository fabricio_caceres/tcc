<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modulo_tomada extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->output->set_content_type('application/json');
    $this->output->set_header('Access-Control-Allow-Origin: *');
    $this->output->set_header('Access-Control-Allow-Headers: *');
    $this->load->model('modulo_tomada_model');
  }

  public function listar_modulo_disponivel() {
    $resultado = $this->modulo_tomada_model->listarModuloDisponivel();
    if($resultado["status"] == "erro"){
      $this->output->set_status_header(400);
    }else if($resultado["status"] == "sucesso"){
      $this->output->set_status_header(200);
    }
    $this->output->set_output(json_encode($resultado));
  }

  public function get_dados_byId_aparelho ($id_aparelho, $id_modulo_tomada){
    $resultado = $this->modulo_tomada_model->getDatosByIdAparelho($id_aparelho, $id_modulo_tomada);
    if($resultado["status"] == "erro"){
      $this->output->set_status_header(400);
    }else if($resultado["status"] == "sucesso"){
      $this->output->set_status_header(200);
    }
    $this->output->set_output(json_encode($resultado));
  }
}

/* End of file modulo_tomada.php */
/* Location: ./application/controllers/modulo_tomada.php */