<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periodo_aparelho extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->output->set_content_type('application/json');
    $this->output->set_header('Access-Control-Allow-Origin: *');
    $this->output->set_header('Access-Control-Allow-Headers: *');
    $this->load->model('periodo_aparelho_model');
  }

  public function is_data_modulo_valido() {
    $resultado = $this->periodo_aparelho_model->is_data_modulo_valido($_POST);
    if($resultado["status"] == "erro"){
      $this->output->set_status_header(400);
    }else if($resultado["status"] == "sucesso"){
      $this->output->set_status_header(200);
    }
    $this->output->set_output(json_encode($resultado));
  }
}

/* End of file modulo_tomada.php */
/* Location: ./application/controllers/modulo_tomada.php */