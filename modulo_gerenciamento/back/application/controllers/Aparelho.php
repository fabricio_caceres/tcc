<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aparelho extends CI_Controller {

  function __construct() {
        parent::__construct();

        $this->output->set_content_type('application/json');
        $this->output->set_header('Access-Control-Allow-Origin: *');
        $this->output->set_header('Access-Control-Allow-Headers: *');
        $this->load->model('aparelho_model');
  }

  public function get() {
        $resultado = $this->aparelho_model->get();

        if($resultado["status"] == "erro"){
            $this->output->set_status_header(400);
        }else if($resultado["status"] == "sucesso"){
            $this->output->set_status_header(200);
        }

        $this->output->set_output(json_encode($resultado));
  }

  public function post() {
        $resultado = $this->aparelho_model->post($_POST);

        if($resultado["status"] == "erro"){
            $this->output->set_status_header(400);
        }else if($resultado["status"] == "sucesso"){
            $this->output->set_status_header(200);
        }
        $this->output->set_output(json_encode($resultado));
  }

  public function put() {
        parse_str(file_get_contents("php://input"),$post_vars);
        $resultado = $this->aparelho_model->put($post_vars);

        if($resultado["status"] == "erro"){
            $this->output->set_status_header(400);
        }else if($resultado["status"] == "sucesso"){
            $this->output->set_status_header(200);
        }
        $this->output->set_output(json_encode($resultado));
  }

  public function get_aparelho($id_aparelho) {
        $resultado = $this->aparelho_model->get_aparelho($id_aparelho);

        if($resultado["status"] == "erro"){
            $this->output->set_status_header(400);
        }else if($resultado["status"] == "sucesso"){
            $this->output->set_status_header(200);
        }
        $this->output->set_output(json_encode($resultado));
  }

  public function delete($id_aparelho) {
    $resultado = $this->aparelho_model->delete($id_aparelho);

    if($resultado["status"] == "erro"){
        $this->output->set_status_header(400);
    }else if($resultado["status"] == "sucesso"){
        $this->output->set_status_header(200);
    }
    $this->output->set_output(json_encode($resultado));
  }

  public function total_dia() {
        $resultado = $this->aparelho_model->total_dia($_POST);

        if($resultado["status"] == "erro"){
            $this->output->set_status_header(400);
        }else if($resultado["status"] == "sucesso"){
            $this->output->set_status_header(200);
        }

        $this->output->set_output(json_encode($resultado));
  }

  public function total_ano() {
        $resultado = $this->aparelho_model->total_ano($_POST);

        if($resultado["status"] == "erro"){
           $this->output->set_status_header(400);
        }else if($resultado["status"] == "sucesso"){
            $this->output->set_status_header(200);
        }

        $this->output->set_output(json_encode($resultado));
  }

  public function total_mes() {
        $resultado = $this->aparelho_model->total_mes($_POST);

        if($resultado["status"] == "erro"){
            $this->output->set_status_header(400);
        }else if($resultado["status"] == "sucesso"){
            $this->output->set_status_header(200);
        }

        $this->output->set_output(json_encode($resultado));
  }

  public function total_analisado() {
        $resultado = $this->aparelho_model->total_analisado($_POST);

        if($resultado["status"] == "erro"){
            $this->output->set_status_header(400);
        }else if($resultado["status"] == "sucesso"){
            $this->output->set_status_header(200);
        }

        $this->output->set_output(json_encode($resultado));
  }

  public function aparelho_ativo() {
        $resultado = $this->aparelho_model->aparelho_ativo($_POST);

        if($resultado["status"] == "erro"){
            $this->output->set_status_header(400);
        }else if($resultado["status"] == "sucesso"){
            $this->output->set_status_header(200);
        }

        $this->output->set_output(json_encode($resultado));
  }

  public function get_ultima_analise(){
    $resultado = $this->aparelho_model->get_ultima_analise($_POST);

    if($resultado["status"] == "erro"){
        $this->output->set_status_header(400);
    }else if($resultado["status"] == "sucesso"){
        $this->output->set_status_header(200);
    }

    $this->output->set_output(json_encode($resultado));
  }

  public function teste(){
        $data['corrente'] = (float)rand()/(float)getrandmax();
        $data['tomada'] = 1; //rand(0, 2);
        $data['no_modulo'] = 2;// rand(1, 2);
        $data['data_hora'] = date('Y-m-d H:i:s');
        $this->db->insert('modulo_tomada', $data);
          echo "sucess";
  }

}

/* End of file aparelho.php */
/* Location: ./application/controllers/aparelho.php */
