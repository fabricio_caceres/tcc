<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.k
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Aparelho
// $route['aparelho/(:num)']['get'] = 'aparelho/get/$1';
$route['aparelho']['get'] = 'aparelho/get';
$route['aparelho/(:num)']['get'] = 'aparelho/get_aparelho/$1';
$route['aparelho']['post'] = 'aparelho/post';
$route['aparelho']['put'] = 'aparelho/put';
$route['aparelho/(:num)']['delete'] = 'aparelho/delete/$1';

//Modulo_tomada
$route['modulo_tomada']['get'] = 'modulo_tomada/listar_modulo_disponivel';
$route['modulo_tomada/:num(/:num)']['get'] = 'modulo_tomada/get_dados_byId_aparelho/$1/$1';

//Periodo aparelho
$route['is_data_modulo_valido']['post'] = 'periodo_aparelho/is_data_modulo_valido';