<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aparelho_model extends CI_Model {
    public $id_aparelho;
    public $nome_aparelho;
    public $modelo_aparelho;
    public $hora_uso;
    public $id_inmetro;
    public $id_usuario;
    public $localizacao;

    public function post($data){
        $config = array(
         array(
           'field'   => 'nome_aparelho',
           'label'   => 'Nome Aparelho',
           'rules'   => 'required'
           )
         );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
          $resposta['status'] = 'erro';
          $resposta['msg'] = validation_errors();
        }
        else
        {
          $insert["nome_aparelho"]  = $data["nome_aparelho"];
          $insert["modelo_aparelho"]  = $data["modelo_aparelho"];
          $insert["hora_uso"]  = isset($data["hora_uso"]) && $data["hora_uso"] !== ""? intval($data["hora_uso"]) : 0;
          $insert["localizacao"]    = isset($data["localizacao"])? $data["localizacao"] : null;
          $insert["id_inmetro"] = isset($data["id_inmetro"])? $data["id_inmetro"] : null;
          // $this->id_usuario = isset($data["id_usuario"])? $data["id_usuario"] : null;

          $this->db->trans_begin();

          $this->db->insert('aparelho', $insert);

          if ($this->db->trans_status() === FALSE)
          {
            $this->db->trans_rollback();
            $retorno["status"] = "erro";

            $msg = $this->db->_error_message();
            $num = $this->db->_error_number();
            $retorno['msg'] = "Error(".$num.") ".$msg;
          }
          else
          {
            $this->db->trans_commit();
            $data["id_aparelho"] = $this->db->insert_id();

            if(isset($data["tomada"]) || isset($data["no_modulo"]) || isset($data["dthr_inicio"]) ){
              $this->load->model('periodo_aparelho_model');
              $retorno = $this->periodo_aparelho_model->post($data);
            }else{
              $retorno["status"] = "sucesso";
              $retorno["msg"] = "Aparelho cadastrado com sucesso";
            }

          }
          return $retorno;
        }
    }

    public function put($data){

      $update["nome_aparelho"]  = $data["nome_aparelho"];
      $update["modelo_aparelho"]  = $data["modelo_aparelho"];
      $update["hora_uso"]  = isset($data["hora_uso"])? intval($data["hora_uso"]) : 0;
      $update["localizacao"]    = isset($data["localizacao"])? $data["localizacao"] : null;
      // $update["id_inmetro"] = isset($data["id_inmetro"])? $data["id_inmetro"] : null;
      // $this->id_usuario = isset($data["id_usuario"])? $data["id_usuario"] : null;
      $this->db->trans_begin();
      $this->db->where('id_aparelho', $data['id_aparelho']);
      $this->db->update('aparelho', $update);

      if ($this->db->trans_status() === FALSE)
      {
        $this->db->trans_rollback();
        $retorno["status"] = "erro";

        $msg = $this->db->_error_message();
        $num = $this->db->_error_number();
        $retorno['msg'] = "Error(".$num.") ".$msg;
      }
      else
      {
        $this->db->trans_commit();

        if(isset($data["tomada"]) || isset($data["no_modulo"]) || isset($data["dthr_inicio"]) ){
          $this->load->model('periodo_aparelho_model');
          $retorno = $this->periodo_aparelho_model->put($data);
        }else{
          $retorno["status"] = "sucesso";
          $retorno["msg"] = "Aparelho cadastrado com sucesso";
        }

      }
      return $retorno;

    }

    public function get_aparelho($id_aparelho) {
      if(!isset($id_aparelho) || $id_aparelho == "" ) {
        $retorno['status'] = "erro";
        $retorno['msg'] = "Erro ao editar aparelho (id_aparelho)";
        return $retorno;
      }

      $select = "SELECT a.*, pa.id_periodo, pa.no_modulo, pa.tomada,
         to_char(pa.dthr_inicio, 'YYYY-MM-DD HH24:MI:SS') as dthr_inicio,
         to_char(pa.dthr_fim, 'YYYY-MM-DD HH24:MI:SS') as dthr_fim
                 FROM aparelho a, periodo_aparelho pa
                 WHERE a.id_aparelho = ?
                 AND pa.id_aparelho = a.id_aparelho
                 ORDER BY a.id_aparelho ASC";

      $valores = array($id_aparelho);

      $result = $this->db->query($select, $valores)->result();
      $result = $result? $result[0]: false;

      if($result) {
          $retorno['status'] = "sucesso";
          $retorno['aparelho'] = $result;
      }else {
          $retorno['status'] = "erro";
          $retorno['msg'] = "Nenhum aparelho";
      }

      return $retorno;
    }

    public function get(){
        $select = "SELECT a.id_aparelho, a.nome_aparelho, a.modelo_aparelho, a.hora_uso,
                        a.localizacao, (SELECT count(1)
                                        FROM periodo_aparelho pa
                                        WHERE pa.id_aparelho = a.id_aparelho) as status
                   FROM aparelho a
                   ORDER BY a.id_aparelho ASC";

        $result = $this->db->query($select)->result();

        $retorno['status'] = "sucesso";
        $retorno['aparelhos'] = $result;

        return $retorno;
    }

    public function delete($id_aparelho) {
      if(!isset($id_aparelho) || $id_aparelho == "" ) {
        $retorno['status'] = "erro";
        $retorno['msg'] = "Erro ao deletar aparelho (id_aparelho)";
        return $retorno;
      }
      $this->db->where('id_aparelho', $id_aparelho);
      $this->db->delete('periodo_aparelho');

      $this->db->where('id_aparelho', $id_aparelho);
      if($this->db->delete('aparelho')) {
        $retorno['status'] = "sucesso";
        $retorno['msg'] = "Excluido com sucesso";
      }else{
        $retorno['status'] = "erro";
        $retorno['msg'] = "Erro ao deletar aparelho";
      }

      return $retorno;
    }

    public function total_dia($data){
      $config = array(
                 array(
                       'field'   => 'data_atual',
                       'label'   => 'Data atual',
                       'rules'   => 'required'
                    ),
                 array(
                       'field'   => 'id_aparelho',
                       'label'   => 'ID Aparelho',
                       'rules'   => 'required'
                    ),

              );

      $this->form_validation->set_rules($config);

      if ($this->form_validation->run() == FALSE) {
          $retorno['status'] = 'erro';
          $retorno['msg'] = validation_errors();
          return $retorno;
      }else {
        $this->load->model('periodo_aparelho_model');

        $periodo_analise = $this->periodo_aparelho_model->possui_analise_data($data['data_atual'], $data['id_aparelho']);
        if(!$periodo_analise) {
          $retorno['status'] = 'sucesso';
          $retorno['total_dia'] = 0;
          return $retorno;
        }

        $this->load->model('modulo_tomada_model');

        $kwh_dia = $this->modulo_tomada_model->get_total_dia_kwh($data['data_atual'], $periodo_analise);

        $retorno['status'] = 'sucesso';
        $retorno['total_dia'] = $kwh_dia ?  $kwh_dia : '0 kW' ;
        return $retorno;
      }
    }

    public function total_ano($data) {
      $config = array(
                 array(
                       'field'   => 'data_atual',
                       'label'   => 'Data atual',
                       'rules'   => 'required'
                    ),
                 array(
                       'field'   => 'id_aparelho',
                       'label'   => 'ID Aparelho',
                       'rules'   => 'required'
                    ),
              );

      $this->form_validation->set_rules($config);

      if ($this->form_validation->run() == FALSE) {
          $retorno['status'] = 'erro';
          $retorno['msg'] = validation_errors();
          return $retorno;
      }else {
        $this->load->model('periodo_aparelho_model');

        $periodo_analise = $this->periodo_aparelho_model->possui_analise_data($data['data_atual'], $data['id_aparelho']);
        if(!$periodo_analise) {
          $retorno['status'] = 'sucesso';
          $retorno['total_ano'] = 0;
          return $retorno;
        }

        $this->load->model('modulo_tomada_model');

        $kwh_ano = $this->modulo_tomada_model->get_total_ano_kwh($data['data_atual'], $periodo_analise);

        $retorno['status'] = 'sucesso';
        $retorno['total_ano'] = $kwh_ano;
        return $retorno;

      }
    }

    public function total_mes($data) {
      $config = array(
       array(
        'field'   => 'mes_atual',
         'label'   => 'Data atual',
         'rules'   => 'required'
         ),
       array(
         'field'   => 'id_aparelho',
         'label'   => 'ID Aparelho',
         'rules'   => 'required'
         ),
       );

      $this->form_validation->set_rules($config);

      if ($this->form_validation->run() == FALSE) {
        $retorno['status'] = 'erro';
        $retorno['msg'] = validation_errors();
        return $retorno;
      }else {
        $this->load->model('periodo_aparelho_model');

        $periodo_analise = $this->periodo_aparelho_model->possui_analise_mes($data['mes_atual'], $data['id_aparelho']);
        if(!$periodo_analise) {
          $retorno['status'] = 'sucesso';
          $retorno['total_mes'] = 0;
          return $retorno;
        }

        $this->load->model('modulo_tomada_model');

        $kwh_mes = $this->modulo_tomada_model->get_total_mes_kwh($data['mes_atual'], $periodo_analise);

        $retorno['status'] = 'sucesso';
        $retorno['total_mes'] = $kwh_mes;
        return $retorno;

      }
    }

    public function total_analisado($data) {
      $config = array(
       array(
         'field'   => 'id_aparelho',
         'label'   => 'ID Aparelho',
         'rules'   => 'required'
         ),
       );

      $this->form_validation->set_rules($config);

      if ($this->form_validation->run() == FALSE) {
        $retorno['status'] = 'erro';
        $retorno['msg'] = validation_errors();
        return $retorno;
      }else {
        $this->load->model('periodo_aparelho_model');

        $periodo_analise = $this->periodo_aparelho_model->possui_analise($data['id_aparelho']);
        if(!$periodo_analise) {
          $retorno['status'] = 'sucesso';
          $retorno['total_analisado'] = 0;
          return $retorno;
        }

        $this->load->model('modulo_tomada_model');

        $kwh_analisado = $this->modulo_tomada_model->get_total_analisado_kwh($periodo_analise);

        $retorno['status'] = 'sucesso';
        $retorno['total_analisado'] = $kwh_analisado;
        return $retorno;
      }
    }

    public function aparelho_ativo($data) {
      $config = array(
       array(
         'field'   => 'id_aparelho',
         'label'   => 'ID Aparelho',
         'rules'   => 'required'
         ),
       );

      $this->form_validation->set_rules($config);

      if ($this->form_validation->run() == FALSE) {
        $retorno['status'] = 'erro';
        $retorno['msg'] = validation_errors();
        return $retorno;
      }else {
        $this->load->model('periodo_aparelho_model');

        $periodo_analise = $this->periodo_aparelho_model->possui_analise_data_atual($data['id_aparelho']);
        if(!$periodo_analise) {
          $retorno['status'] = 'sucesso';
          $retorno['total_analisado'] = 0;
          return $retorno;
        }

        $this->load->model('modulo_tomada_model');
        $id_modulo_tomada = isset($data['id_modulo_tomada']) ? $data['id_modulo_tomada'] : 0;
        $kw_analisado = $this->modulo_tomada_model->get_analise_data_atual($periodo_analise, $id_modulo_tomada);

        $retorno['status'] = 'sucesso';
        $retorno['kw_analisado'] = $kw_analisado;
         return $retorno;
      }
    }

    public function get_ultima_analise($data){
      if(!isset($data['id_aparelho'])){
        $resposta['status'] = 'erro';
        $resposta['msg'] = 'O campo ID Aparelho é obrigatorio';
        return $resposta;
      }

      $this->load->model('periodo_aparelho_model');

      $periodo_analise = $this->periodo_aparelho_model->possui_analise($data['id_aparelho']);
      if(!$periodo_analise) {
        $retorno['status'] = 'sucesso';
        $retorno['total_analisado'] = 0;
        return $retorno;
      }

      $this->load->model('modulo_tomada_model');
      $kw_analisado = $this->modulo_tomada_model->get_analise($periodo_analise);

      $retorno['status'] = 'sucesso';
      $retorno['kw_analisado'] = $kw_analisado;
       return $retorno;
    }
}

/* End of file aparelho_model.php */
/* Location: ./application/models/aparelho_model.php */
