<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periodo_aparelho_model extends CI_Model {
    private $id_periodo_aparelho;
    private $tomada;
    private $no_modulo;
    private $id_aparelho;
    private $dthr_inicio;
    private $dthr_fim;

    public function post($data) {
        $config = array(
                   array(
                         'field'   => 'tomada',
                         'label'   => 'Numero Tomada',
                         'rules'   => 'required'
                   ),
                   array(
                         'field'   => 'no_modulo',
                         'label'   => 'Numero do Modulo',
                         'rules'   => 'required'
                    ),
                   array(
                         'field'   => 'dthr_inicio',
                         'label'   => 'Data Inicial',
                         'rules'   => 'required'
                    ),

                );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            $resposta['status'] = 'erro';
            $resposta['msg'] = validation_errors();
        }
        else {
            $insert["tomada"] = $data["tomada"];
            $insert["no_modulo"] = $data["no_modulo"];
            $insert["id_aparelho"] = $data["id_aparelho"];
            $insert["dthr_inicio"] = $data["dthr_inicio"];
            $insert["dthr_fim"] = isset($data["dthr_fim"])?$data["dthr_fim"]:null;

            $this->db->trans_begin();

            $this->db->insert('periodo_aparelho', $insert);

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $resposta["status"] = "erro";

                $msg = $this->db->_error_message();
                $num = $this->db->_error_number();
                $resposta['msg'] = "Error(".$num.") ".$msg;
            }
            else {
                $this->db->trans_commit();
                $resposta["status"] = "sucesso";
            }
        }

        return $resposta;
    }

    public function put($data) {

      $update["tomada"] = $data["tomada"];
      $update["no_modulo"] = $data["no_modulo"];
      $update["id_aparelho"] = $data["id_aparelho"];
      $update["dthr_inicio"] = $data["dthr_inicio"];
      $update["dthr_fim"] = isset($data["dthr_fim"]) && $data["dthr_fim"] != "" ?$data["dthr_fim"]:null;

      $this->db->trans_begin();

      $this->db->where('id_periodo', $data['id_periodo']);
      $this->db->update('periodo_aparelho', $update);

      if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $resposta["status"] = "erro";

          $msg = $this->db->_error_message();
          $num = $this->db->_error_number();
          $resposta['msg'] = "Error(".$num.") ".$msg;
      }
      else {
          $this->db->trans_commit();
          $resposta["status"] = "sucesso";
      }
      return $resposta;
    }

    public function is_data_modulo_valido($data){
        $config = array(
                   array(
                         'field'   => 'tomada',
                         'label'   => 'Numero Tomada',
                         'rules'   => 'required'
                   ),
                   array(
                         'field'   => 'no_modulo',
                         'label'   => 'Numero do Modulo',
                         'rules'   => 'required'
                    ),
                   array(
                         'field'   => 'dthr_inicio',
                         'label'   => 'Data Inicial',
                         'rules'   => 'required'
                    ),

                );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
           $resposta['status'] = 'erro';
           $resposta['msg'] = validation_errors();
        }else{
            $this->dthr_inicio = $data["dthr_inicio"];
            $this->dthr_fim = isset($data["dthr_fim"])?$data["dthr_fim"]:null;
            $this->no_modulo = $data["no_modulo"];
            $this->tomada = $data["tomada"];
            $valores = array($this->no_modulo, $this->tomada, $this->dthr_inicio);

            $q = "SELECT id_periodo
                      FROM periodo_aparelho pa
                      WHERE pa.no_modulo = ?
                        AND pa.tomada = ?
                        AND to_char(pa.dthr_inicio, 'YYYY-MM-DD HH24:MI') = ?";

            if(isset($this->dthr_fim) && $this->dthr_fim  != ""){
                $q .= " AND to_char(pa.dthr_fim, 'YYYY-MM-DD HH24:MI') = ?";
                array_push($valores, $this->dthr_fim);
            }
            $resultado = $this->db->query($q, $valores)->result();

            if(count($resultado) <= 0){
                $resposta['status'] = 'sucesso';
            }else{
                $resposta['status'] = 'erro';
                $resposta['msg'] = 'Já existe um aparelho nesse range';
            }
        }

        return $resposta;
    }

    public function possui_analise_data($data_atual, $id_aparelho){
        if($data_atual == '' || !isset($data_atual)){
            return false;
        }

        $query = "SELECT pa.*, a.hora_uso
                      FROM periodo_aparelho pa, aparelho a
                      WHERE pa.id_aparelho = ?
                      AND pa.id_aparelho = a.id_aparelho
                      AND to_char(pa.dthr_inicio, 'YYYY-MM-DD') <= ?
                      AND CASE WHEN pa.dthr_fim is not NULL
                        THEN to_char(pa.dthr_fim, 'YYYY-MM-DD') >= ?
                      ELSE true
                      END";

        $valores = array($id_aparelho, $data_atual, $data_atual);
        $analise_data = $this->db->query($query, $valores)->result();
        return $analise_data? $analise_data[0]: false;
    }

    public function possui_analise_mes($mes_atual, $id_aparelho){
        if($mes_atual == '' || !isset($id_aparelho)){
            return false;
        }

        $query = "SELECT pa.*, a.hora_uso
                      FROM periodo_aparelho pa, aparelho a
                      WHERE pa.id_aparelho = ?
                       AND pa.id_aparelho = a.id_aparelho
                      AND to_char(pa.dthr_inicio, 'YYYY-MM') <= ?
                      AND CASE WHEN pa.dthr_fim is not NULL
                        THEN to_char(pa.dthr_fim, 'YYYY-MM') >= ?
                      ELSE true
                      END";

        $valores = array($id_aparelho, $mes_atual, $mes_atual);
        $analise_data = $this->db->query($query, $valores)->result();
        return $analise_data? $analise_data[0]: false;
    }

    public function possui_analise($id_aparelho){
        if(!isset($id_aparelho)){
            return false;
        }

        $query = "SELECT pa.*, a.hora_uso
          FROM periodo_aparelho pa, aparelho a
          WHERE pa.id_aparelho = ?
           AND pa.id_aparelho = a.id_aparelho";

        $valores = array($id_aparelho);
        $analise_data = $this->db->query($query, $valores)->result();
        return $analise_data? $analise_data[0]: false;
    }

    public function possui_analise_data_atual($id_aparelho){
        if(!isset($id_aparelho)){
            return false;
        }
        $query = "SELECT pa.*, a.hora_uso
                      FROM periodo_aparelho pa, aparelho a
                      WHERE pa.id_aparelho = ?
                      AND pa.id_aparelho = a.id_aparelho
                      AND to_char(pa.dthr_inicio, 'YYYY-MM-DD') <= ?
                      AND CASE WHEN pa.dthr_fim is not NULL
                        THEN to_char(pa.dthr_fim, 'YYYY-MM-DD') >= ?
                      ELSE true
                      END";

          $valores = array($id_aparelho, date('Y-m-d'), date('Y-m-d'));
          $analise_data = $this->db->query($query, $valores)->result();
          return $analise_data? $analise_data[0]: false;
    }
}

/* End of file periodo_aparelho_model.php */
/* Location: ./application/models/periodo_aparelho_model.php */
