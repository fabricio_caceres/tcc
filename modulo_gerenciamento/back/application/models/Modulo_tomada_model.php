<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modulo_tomada_model extends CI_Model {
  private $id_modulo_tomada;
  private $no_modulo;
  private $tomada;
  private $corrente;
  private $data_hora;

  //Função que retorna os modulos e tomadas dipsoniveis
  public function listarModuloDisponivel(){
        $query = "SELECT mt.no_modulo, mt.tomada
                        FROM modulo_tomada mt
                        GROUP BY
                          mt.no_modulo, mt.tomada
                        ORDER BY
                          mt.no_modulo, mt.tomada ASC";

        $resultado = $this->db->query($query)->result();
        if(count($resultado) < 0){
            $retorno["status"] = "erro";
            $retorno["modulos"] = "Nenhum modulo disponivel";
        }
        $retorno["status"] = "sucesso";
        $retorno["modulos"] = $resultado;
        return $retorno;
  }

  public function getDatosByIdAparelho($id_aparelho, $id_modulo_tomada){

        $select = "
          SELECT mt.*
          FROM modulo_tomada mt, periodo_aparelho pa
          WHERE pa.id_aparelho =".$id_aparelho."
          AND mt.no_modulo = pa.no_modulo
          AND mt.tomada = pa.tomada
          AND mt.data_hora >= pa.dthr_inicio
          AND mt.data_hora <= pa.dthr_fim";

        if(isset($id_modulo_tomada)){
            $select.=" AND mt.id_modulo_tomada > ".$id_modulo_tomada;
        }
        $select.=" ORDER BY mt.id_periodo_aparelho";

        $resultado = $this->db->query($select)->result();
        $retorno["status"] = "sucesso";
        $retorno["modulo_tomada"] = $resultado;
        return $resultado;
  }

  public function get_total_dia_kwh($data_analise, $periodo_analise){
          if(!isset($data_analise) || !isset($periodo_analise)){
              return false;
          }

          $query = "SELECT mt.corrente, to_char(mt.data_hora, 'DD/MM/YYYY HH24:MI:SS')as data_hora
                          FROM modulo_tomada mt
                          WHERE to_char(mt.data_hora, 'YYYY-MM-DD') = ?
                          AND to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') >= to_char(timestamp?, 'YYYY-MM-DD HH24:MI:SS')
                          AND mt.tomada = ?
                          AND mt.no_modulo = ?";

          $valores = array($data_analise, $periodo_analise->dthr_inicio, $periodo_analise->tomada, $periodo_analise->no_modulo);

          if($periodo_analise->dthr_fim != ""){
            $query .= " AND  to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') <= to_char(timestamp?, 'YYYY-MM-DD HH24:MI:SS')";
            array_push($valores, $periodo_analise->dthr_fim);
          }
          $corrente_aparelho = $this->db->query($query, $valores)->result();

          if(!$corrente_aparelho){
             return false;
          }
          $this->load->model('modulo_fase_model');
          $tensao_dia = $this->modulo_fase_model->get_tensao_dia($data_analise);
          if(!$tensao_dia){
              return false;
          }

          $kw_aparelho = 0;
          $total_horas = 0;
          $datas = array();
          foreach ($corrente_aparelho as $corrente) {
              $kw_aparelho = $kw_aparelho + ($corrente->corrente*intval($tensao_dia));
              array_push($datas, $corrente->data_hora);
          }

          $this->load->helper('data_hora_helper');
          $hora = get_hora_data($datas, $periodo_analise->hora_uso);
          $media = $kw_aparelho/ count($corrente_aparelho);
          $kwh = ($media/1000)*$hora;

          // if($kwh > 0){
            // $kwh = number_format($media*$hora, 2, ",", ".")." wh";
          // }else{
            $kwh = number_format($kwh, 2, ",", ".")." kWh";
          // }
          return $kwh;
  }

  public function get_total_ano_kwh($data_analise, $periodo_analise) {
        if(!isset($data_analise) || !isset($periodo_analise)){
            return false;
        }


        $query = "SELECT mt.corrente, to_char(mt.data_hora, 'DD/MM/YYYY HH24:MI:SS')as data_hora
                        FROM modulo_tomada mt
                        WHERE to_char(mt.data_hora, 'YYYY') = to_char(DATE?, 'YYYY')
                        AND to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') >= to_char(timestamp?, 'YYYY-MM-DD HH24:MI:SS')
                        AND mt.tomada = ?
                        AND mt.no_modulo = ?";

        $valores = array($data_analise, $periodo_analise->dthr_inicio , $periodo_analise->tomada, $periodo_analise->no_modulo);

        if($periodo_analise->dthr_fim != ""){
            $query .= " AND  to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') <= to_char(timestamp?, 'YYYY-MM-DD HH24:MI:SS')";
            array_push($valores, $periodo_analise->dthr_fim);
        }
        $corrente_aparelho = $this->db->query($query, $valores)->result();

        if(!$corrente_aparelho){
            return false;
        }
        $this->load->model('modulo_fase_model');

        $kw_aparelho = 0;
        $total_horas = 0;
        $datas = array();
        foreach ($corrente_aparelho as $corrente) {
            $data_atual = DateTime::createFromFormat('d/m/Y H:i:s', $corrente->data_hora);
            $tensao_dia = $this->modulo_fase_model->get_tensao_dia($data_atual->format('Y-m-d'));
            $kw_aparelho = $kw_aparelho + ($corrente->corrente*intval($tensao_dia));

            array_push($datas, $corrente->data_hora);
        }
        $this->load->helper('data_hora_helper');
        $hora = get_hora_data($datas, $periodo_analise->hora_uso);
        $media = $kw_aparelho/ count($corrente_aparelho);
        $kwh = ($media/1000)*$hora;
        // if($kwh > 0){
          // $kwh = number_format($media*$hora, 2, ",", ".")." wh";
        // }else{
          $kwh = number_format($kwh, 2, ",", ".")." kWh";
        // }
        return $kwh;
  }

  public function get_total_mes_kwh($mes_atual, $periodo_analise) {
        if(!isset($mes_atual) || !isset($periodo_analise)){
          return false;
        }

        $query = "SELECT mt.corrente, to_char(mt.data_hora, 'DD/MM/YYYY HH24:MI:SS')as data_hora
                    FROM modulo_tomada mt
                    WHERE  to_char(mt.data_hora, 'YYYY-MM') = ?
                    AND  to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') >=  to_char(timestamp?, 'YYYY-MM-DD HH24:MI:SS')
                    AND mt.tomada = ?
                    AND mt.no_modulo = ?";
        $valores = array($mes_atual, $periodo_analise->dthr_inicio, $periodo_analise->tomada, $periodo_analise->no_modulo);
        if($periodo_analise->dthr_fim != ""){
            $query .= " AND  to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') <= to_char(timestamp?, 'YYYY-MM-DD HH24:MI:SS')";
            array_push($valores, $periodo_analise->dthr_fim);
        }
        $corrente_aparelho = $this->db->query($query, $valores)->result();

        if(!$corrente_aparelho){
           return false;
        }
        $this->load->model('modulo_fase_model');

        $kw_aparelho = 0;
        $total_horas = 0;
        $datas = array();
        foreach ($corrente_aparelho as $corrente) {
            $data_atual = DateTime::createFromFormat('d/m/Y H:i:s', $corrente->data_hora);
            $tensao_dia = $this->modulo_fase_model->get_tensao_dia($data_atual->format('Y-m-d'));
            $kw_aparelho = $kw_aparelho + ($corrente->corrente*intval($tensao_dia));

            array_push($datas, $corrente->data_hora);
        }
        $this->load->helper('data_hora_helper');
        $hora = get_hora_data($datas, $periodo_analise->hora_uso);
        $media = $kw_aparelho/ count($corrente_aparelho);
        $kwh = ($media/1000)*$hora;
        // if($kwh > 0){
          // $kwh = number_format($media*$hora, 2, ",", ".")." wh";
        // }else{
          $kwh = number_format($kwh, 2, ",", ".")." kWh";
        // }
        return $kwh;
  }

  public function get_total_analisado_kwh($periodo_analise) {
        if(!isset($periodo_analise)){
            return false;
        }

        $query = "SELECT mt.corrente, to_char(mt.data_hora, 'DD/MM/YYYY HH24:MI:SS') as data_hora
                        FROM modulo_tomada mt
                        WHERE  to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') >= to_char(timestamp?, 'YYYY-MM-DD HH24:MI:SS')";

        $valores = array($periodo_analise->dthr_inicio);

        if($periodo_analise->dthr_fim != ""){
            $query .= " AND  to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') <= to_char(timestamp?, 'YYYY-MM-DD HH24:MI:SS')";
            array_push($valores, $periodo_analise->dthr_fim);
        }

        $query .= "  AND mt.tomada = ? AND mt.no_modulo = ?";
        array_push($valores, $periodo_analise->tomada);
        array_push($valores, $periodo_analise->no_modulo);
        $corrente_aparelho = $this->db->query($query, $valores)->result();

        if(!$corrente_aparelho){
            return false;
        }
        $this->load->model('modulo_fase_model');

        $kw_aparelho = 0;
        $total_horas = 0;
        $datas = array();
        foreach ($corrente_aparelho as $corrente) {
            $data_atual = DateTime::createFromFormat('d/m/Y H:i:s', $corrente->data_hora);
            $tensao_dia = $this->modulo_fase_model->get_tensao_dia($data_atual->format('Y-m-d'));
            $kw_aparelho = $kw_aparelho + ($corrente->corrente*intval($tensao_dia));

            array_push($datas, $corrente->data_hora);
        }
        $this->load->helper('data_hora_helper');
        $hora = get_hora_data($datas, $periodo_analise->hora_uso);
        $media = $kw_aparelho/ count($corrente_aparelho);
        $kwh = ($media/1000)*$hora;
        // if($kwh > 0){
          // $kwh = number_format($media*$hora, 2, ",", ".")." wh";
        // }else{
          $kwh = number_format($kwh, 2, ",", ".")." kWh";
        // }
        return $kwh;
  }

  public function get_analise_data_atual($periodo_analise, $id_modulo_tomada) {
        if(!isset($periodo_analise)){
            return false;
        }

        $query = "SELECT mt.id_modulo_tomada, mt.corrente, to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') as data_hora
                          FROM modulo_tomada mt
                          WHERE mt.id_modulo_tomada in
                              (SELECT m.id_modulo_tomada FROM modulo_tomada m
                                WHERE  to_char(m.data_hora, 'YYYY-MM-DD HH24:MI') <= to_char(timestamp?, 'YYYY-MM-DD HH24:MI')
                                AND to_char(m.data_hora, 'YYYY-MM-DD HH24:MI') >= to_char((timestamp? - INTERVAL '3 hours'), 'YYYY-MM-DD HH24:MI')
                                AND m.tomada = ?
                                AND m.no_modulo = ?";

          $valores = array(date("Y-m-d H:i"), date("Y-m-d H:i"), $periodo_analise->tomada, $periodo_analise->no_modulo);
          if($id_modulo_tomada > 0){
              $query .= " AND m.id_modulo_tomada > ?";
              array_push($valores, $id_modulo_tomada);
          }
          $query .="ORDER BY m.id_modulo_tomada DESC) ORDER BY mt.id_modulo_tomada ASC LIMIT 1000";

          $resultado =  $this->db->query($query, $valores)->result();

          if($resultado){
            $this->load->model('modulo_fase_model');
            $tensao_dia = $this->modulo_fase_model->get_tensao_dia("");
            if(!$tensao_dia){
                return false;
            }

            foreach ($resultado as $re) {
                $re->corrente = $re->corrente*intval($tensao_dia);
            }

          }

          return $resultado;
  }

  public function get_analise($periodo_analise) {
        if(!isset($periodo_analise)){
            return false;
        }

        $query = "SELECT mt.id_modulo_tomada, mt.corrente,
                    to_char(mt.data_hora, 'YYYY-MM-DD HH24:MI:SS') as data_hora
                          FROM modulo_tomada mt
                          WHERE mt.id_modulo_tomada in
                              (SELECT m.id_modulo_tomada FROM modulo_tomada m
                                WHERE  to_char(m.data_hora, 'YYYY-MM-DD HH24:MI') >= to_char(timestamp?, 'YYYY-MM-DD HH24:MI')
                                AND to_char(m.data_hora, 'YYYY-MM-DD HH24:MI') <= to_char(timestamp?, 'YYYY-MM-DD HH24:MI')
                                AND m.tomada = ?
                                AND m.no_modulo = ?";

          $valores = array($periodo_analise->dthr_inicio,
            $periodo_analise->dthr_fim? $periodo_analise->dthr_fim : date("Y-m-d H:i"), $periodo_analise->tomada, $periodo_analise->no_modulo);

          $query .="ORDER BY m.id_modulo_tomada DESC) ORDER BY mt.id_modulo_tomada ASC LIMIT 1000";

          $resultado =  $this->db->query($query, $valores)->result();

          if($resultado){
            $this->load->model('modulo_fase_model');
            $tensao_dia = $this->modulo_fase_model->get_tensao_dia("");
            if(!$tensao_dia){
                return false;
            }

            foreach ($resultado as $re) {
                $re->corrente = $re->corrente*intval($tensao_dia);
            }

          }

          return $resultado;
  }

}

/* End of file modulo_tomada_model.php */

