<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modulo_fase_model extends CI_Model {
  public $id_modulo_fase, $dthr, $fase_1, $fase_2, $fase_3, $tensao;

  public function get_tensao_dia($data_analise){
    $query = "SELECT tensao
              FROM modulo_fase
              ORDER BY id_modulo_fase DESC
              LIMIT 1";

    $tensao = $this->db->query($query)->result();
    return $tensao? $tensao[0]->tensao: false;
  }


}

/* End of file Modulo_fase_model.php */
/* Location: ./application/models/Modulo_fase_model.php */