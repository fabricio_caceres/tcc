'use strict';

var ApplicationController = new function () {

    this.breadcrumb = function ($scope, $state, breadcrumb) {

        $scope.$on('updateBreadcrumb', function () {
            $scope.breadcrumb = breadcrumb.data;
        });
    };

    this.menubar = function ($scope, $rootScope, $state, breadcrumb) {

        $scope.modules = [];

        var module = {
            display: 'Área de Trabalho',
            icon: 'fa fa-dashboard',
            url: '#/dashboard'
        };

        $scope.modules.push(module);

        var module = {
            display: 'Aparelho',
            icon: 'fa fa-plug',
            url: 'aparelho'
        };

        $scope.modules.push(module);

        // var module = {
        //     display: 'PID',
        //     icon: 'fa fa-envelope',
        //     url: '#/pid'
        // };

        // $scope.modules.push(module);

        // var module = {
        //     display: 'SAS',
        //     icon: 'fa fa-building',
        //     url: '#/sas'
        // };

        // $scope.modules.push(module);

        $scope.active = 0;

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            angular.forEach($scope.modules, function (module, i) {

                if (toState.url.indexOf(module.url.substring(1)) >= 0) {
                    $scope.active = i;
                }
            });
        });
    };


    // this.load = function ($scope, $state, title, breadcrumb, $location, authService) {

    //     breadcrumb.set({
    //         title: 'Área de trabalho',
    //         description: 'Painel geral',
    //         icon: 'fa fa-dashboard',
    //           itens: [
    //         { href: '', display: 'Ínicio' },
    //         { href: '#/dashboard', display: 'Área de trabalho' },
    //           ]
    //     });

    // };
}