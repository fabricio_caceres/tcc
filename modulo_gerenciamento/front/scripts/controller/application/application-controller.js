'use strict';

var ApplicationController = new function () {

    Application.controller('ApplicationController.breadcrumb', function ($scope, $state, breadcrumb) {

        $scope.$on('updateBreadcrumb', function () {
            $scope.breadcrumb = breadcrumb.data;
        });
    });

    Application.controller('ApplicationController.menubar', function ($scope, $rootScope, $state, breadcrumb)
    {
       $scope.modules = [];

             var module = {
               nome: 'Dashboard',
               icon: 'fa fa-dashboard',
               url: 'dashboard'
             };

             $scope.modules.push(module);

             var module = {
                 nome: 'Aparelho',
                 icon: 'fa fa-plug',
                 url: 'aparelho'
             };

             $scope.modules.push(module);
    });

    // this.menubar = function ($scope, $rootScope, $state, breadcrumb) {

    //   $scope.modules = [];

    //   var module = {
    //     display: 'Dashboard',
    //     icon: 'fa fa-dashboard',
    //     url: 'dashboard'
    //   };

    //   $scope.modules.push(module);

    //   var module = {
    //       display: 'Aparelho',
    //       icon: 'fa fa-plug',
    //       url: 'aparelho'
    //   };

    //   $scope.modules.push(module);
    // };

    // this.load = function ($scope) {

    // };
};