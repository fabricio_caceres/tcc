'use strict';

var InmetroController = new function () {

  this.load = function ($scope, rest, aparelho_inmetro, $injector) {
    $scope.aparelho_inmetro = [];
    $scope.view = '';
    $scope.configuracao.templates =[
      {id_div: "id_viewAdicionar",    url_template: API.templatePath+"inmetro/add.html"},
      {id_div: "id_viewListar", url_template: API.templatePath+"inmetro/listar.html"}
    ];

    $scope.loadListar = function () {
      $scope.view = 'id_viewListar';

      rest.get('inmetro')
        .success(function (data) {
          if(data.status=="sucesso") {
            $scope.aparelho_inmetro  = data.aparelho_inmetro;
          }else{
            console.log(data);
          }
        })
        .error( function (data) {
          console.log(data);
        });
    };

    $scope.loadListar();

  };
};