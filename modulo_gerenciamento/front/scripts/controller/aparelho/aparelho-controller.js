'use strict';

var AparelhoController = new function () {

    this.load = function ($scope, $rootScope, rest, aparelhos, $injector, $q, $filter, breadcrumb, msgBox) {
        $scope.configuracao = [];
        $scope.func = '';
        $scope.view = 'id_viewListar';
        $scope.configuracao.templates =[
        {id_div: "id_viewAdicionar",    url_template: API.templatePath+"aparelho/add.html"},
        {id_div: "id_viewListar", url_template: API.templatePath+"aparelho/listar.html"},
        {id_div: "id_viewAnalise", url_template: API.templatePath+"aparelho/analise.html"}
        ];

        $scope.aparelhos = aparelhos;

        breadcrumb.set({
            title: 'Aparelho',
            description: 'Gerenciamento dos Aparelhos',
            icon: 'fa fa-plug',
            itens: [
            { ui: 'aparelho', display: 'Aparelhos' },
            ]
        });

        $scope.loadListar = function(){
            $scope.view = 'id_viewListar';
            $scope.aparelho = {};
            $scope.reset();

            var itens = [
              { ui: 'aparelho', display: 'Aparelhos' },
            ];

            breadcrumb.data.itens = itens;

            rest.get('aparelho')
              .success(function (data) {
                  if(data.status=="sucesso") {
                    $scope.aparelhos = data.aparelhos;
                  }else{
                    msgBox.error("Erro ao listar", data.msg);
                  }
              })
              .error( function (data) {
                  msgBox.error("Erro ao listar", data.msg);
              });
        };


        $scope.getModulo = function(){
            rest.get('modulo_tomada')
                .success(function (data) {
                  if(data.status=="sucesso") {
                    $scope.modulos_tomadas = data.modulos;
                    angular.forEach(data.modulos, function(value, key){
                      if(this.indexOf(value.no_modulo) === -1){
                        this.push(value.no_modulo);
                      }
                    }, $scope.modulos);
                    $("#no_modulo").prop('disabled', false);
                  }else{
                    msgBox.error("Erro ao buscar Modulo Tomada", data.msg);
                  }
                })
                .error( function (data) {
                  msgBox.error("Erro ao buscar Modulo Tomada", data.msg);
                });
        };

        $scope.removeAparelho = function (id_aparelho) {
          swal({
              title: "Excluir",
              text: "Tem certeza que deseja excluir o Aparelho?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Sim",
              closeOnConfirm: false,
              closeButtonText: "Não",
               },
              function(){
                rest.delete('aparelho/'+id_aparelho)
                  .success(function (data){
                      if(data.status =="sucesso"){
                          msgBox.success("Sucesso", data.msg);

                          setTimeout(function () {
                            $scope.loadListar();
                          }, 2000);
                      }else{
                          msgBox.error("Erro", data.msg);
                      }
                  })
                  .error(function (data){
                      msgBox.error("Erro", data.msg);
                  });

          });
        };

        $scope.$watch(
              function () { return $scope.aparelho; },

              function (cValue, oValue) {

                  if (undefined !== $scope.aparelho &&  $scope.aparelho.id_aparelho > 0) {

                    if($scope.func== 'analise'){
                      var itens = [
                        { ui: 'aparelho', display: 'Aparelhos' },
                        { ui: 'aparelho', display: 'Analise' },
                        { ui: 'aparelho', display: $scope.aparelho.nome_aparelho }

                      ];

                      if($scope.aparelho.hora_uso){
                        $scope.aparelho.hora_uso = parseInt($scope.aparelho.hora_uso);
                      }

                      if($scope.aparelho.dthr_fim !== undefined && $scope.aparelho.dthr_fim !== null) {
                          itens.push({ ui: 'aparelho',
                              display: ''+ moment(moment(''+$scope.aparelho.dthr_inicio).format('DD/MM/YY') + ' à '+$scope.aparelho.dthr_fim).format('DD/MM/YY') });
                      }else{
                          itens.push({ ui: 'aparelho',
                            display: 'Data Inicial : '+ moment(''+$scope.aparelho.dthr_inicio).format('DD/MM/YY') });
                      }


                      breadcrumb.data.itens = itens;

                      var parametros = {};
                      parametros. id_aparelho = $scope.id_aparelho_analisado;
                      $scope.view = 'id_viewAnalise';
                      rest.post('aparelho/aparelho_ativo',parametros)
                        .success(function (data) {
                            if(data.status=="sucesso") {
                                if(data.kw_analisado.length > 0){
                                  $scope.aparelho_ativo= data.kw_analisado;
                                  if($scope.first_execucao){
                                      $scope.ativo = true;
                                      $scope.chart.nome = "APARELHO EM USO (MODULO: "+$scope.aparelho.no_modulo+" TOMADA: "+$scope.aparelho.tomada+")";
                                      $scope.preparaGraficoUso();
                                      $scope.first_execucao = false;
                                      $scope.iniciaAnalise();
                                  }
                                }else{
                                  rest.post('aparelho/get_ultima_analise',parametros)
                                    .success(function (data) {
                                        if(data.status=="sucesso") {
                                          if(data.kw_analisado.length > 0){
                                            $scope.aparelho_ativo= data.kw_analisado;
                                            $scope.view = 'id_viewAnalise';
                                            $scope.chart.nome = "ULTIMOS REGISTROS (MODULO: "+$scope.aparelho.no_modulo+" TOMADA: "+$scope.aparelho.tomada+")";
                                            $scope.preparaGraficoUso();
                                            $scope.first_execucao = false;
                                            $scope.iniciaAnalise();
                                          }else{
                                            msgBox.error("Erro", "Não possui dados analisados");
                                            $scope.loadListar();
                                          }

                                        }
                                    })
                                    .error( function (data) {
                                       msgBox.error("Erro", "Não possui dados analisados");
                                       $scope.loadListar();
                                    });
                                }

                            }else{
                                msgBox.error("Erro", "Não possui dados analisados");
                                $scope.loadListar();
                            }
                        })
                        .error( function (data) {
                            msgBox.error("Erro", "Não possui dados analisados");
                            $scope.loadListar();
                        });
                    }else if($scope.func == 'edit'){
                      if($scope.aparelho.dthr_fim !== undefined && $scope.aparelho.dthr_fim !== null) {
                          $scope.dthr_fim = moment(''+$scope.aparelho.dthr_fim);
                      }

                      if($scope.aparelho.hora_uso) {
                          $scope.aparelho.hora_uso = parseInt($scope.aparelho.hora_uso);
                      }
                      if($scope.aparelho.no_modulo){
                        $("input#no_modulo").prop('disabled', true);
                      }

                      if($scope.aparelho.tomada) {
                        $("input#tomada").prop('disabled', true);
                      }

                      var itens = [
                        { ui: 'aparelho', display: 'Aparelhos' },
                        { ui: 'aparelho', display: 'Editar' },
                        { ui: 'aparelho', display: $scope.aparelho.nome_aparelho }
                      ];

                      breadcrumb.data.itens = itens;
                      $scope.dthr_inicio = moment(''+$scope.aparelho.dthr_inicio);
                      $('#dthr_inicio').daterangepicker({
                          startDate: $scope.dthr_inicio,
                          timePicker: true,
                          singleDatePicker: true,
                          minDate: '01/01/1970',
                          locale: API.localeDateRangePicker()
                      },
                      function (start) {
                          $scope.dthr_inicio = start;
                      });
                    }
                  }
              }
        );

        $injector.invoke(AparelhoController.loadSalvar, this, {
            $scope: $scope,
            rest: rest,
            $q: $q,
            msgBox: msgBox,
            breadcrumb: breadcrumb,
            $rootScope: $rootScope });
        $injector.invoke(AparelhoController.loadAnalise, this, {
            $scope: $scope,
            rest: rest,
            $q: $q,
            $filter:$filter,
            breadcrumb: breadcrumb,
            msgBox: msgBox});

        $scope.getModulo();
    };

    this.loadSalvar = function ($scope, rest, $q, $filter, msgBox, breadcrumb, $rootScope){
          $scope.viewConfig = [];
          $scope.aparelho = {};
          $scope.modulos_tomadas = [];
          $scope.modulos = [];
          $scope.tomadas = [];
          $scope.range = [];

          $scope.add = function() {
              $scope.view = 'id_viewAdicionar';
              $("#no_modulo").prop('disabled', false);
              $("#tomada").prop('disabled', false);

              breadcrumb.data.itens.push({ ui: '', display: 'Adicionar' });
              $scope.dthr_inicio = moment();
              $('#dthr_inicio').daterangepicker({
                  startDate: $scope.dthr_inicio,
                  timePicker: true,
                  singleDatePicker: true,
                  minDate: '01/01/1970',
                  locale: API.localeDateRangePicker()
              },
              function (start) {
                  $scope.dthr_inicio = start;
              });

              // $scope.getModulo();
          };

          $scope.editar = function (id_aparelho) {
            $scope.view = 'id_viewAdicionar';
            $scope.getAparelho(id_aparelho);
            $scope.func = 'edit';

          };

          $scope.getAparelho = function (id_aparelho) {
              var deferred = $q.defer();

              rest.get('aparelho/'+id_aparelho)
                .success(function (data) {
                  if(data.status=="sucesso") {
                    $scope.aparelho= data.aparelho;
                    deferred.resolve(data.aparelho);
                  }else{
                    deferred.reject(data);
                    msgBox.error("Erro ao buscar aparelho", data.msg);
                  }
                })
                .error( function (data) {
                    deferred.reject(data);
                    msgBox.error("Erro ao buscar aparelho", data.msg);
                });
              return deferred.promise;
          };

          $scope.start_dthr_fim = function () {
              if($scope.dthr_fim === undefined){
                $scope.dthr_fim = moment();
              }

              $('#dthr_fim').daterangepicker({
                  startDate: $scope.dthr_fim,
                  timePicker: true,
                  singleDatePicker: true,
                  minDate: '01/01/1970',
                  locale: API.localeDateRangePicker()
              },
              function (end) {
                  $scope.dthr_fim = end;
              });
          };

          $scope.validacao = function(){
              if($scope.aparelho.hora_uso > 24) {
                msgBox.error("Erro", "Hora deve ser menor que 24");
                return;
              }

              if(($scope.aparelho.dthr_inicio) && ($scope.aparelho.dthr_inicio == $scope.dthr_inicio.format('YYYY-MM-DD HH:mm:ss'))) {
                if($scope.aparelho.id_aparelho && $scope.aparelho.id_aparelho > 0){
                  $scope.atualizar();
                  return;
                }
              }else{
                $scope.aparelho.dthr_inicio = $scope.dthr_inicio.format('YYYY-MM-DD HH:mm:ss');
              }

              rest.post('is_data_modulo_valido', $scope.aparelho)
                .success(function (data){
                    if(data.status =="sucesso"){
                      if($scope.aparelho.id_aparelho && $scope.aparelho.id_aparelho > 0){
                        $scope.atualizar();
                      }else{
                        $scope.adicionar();
                      }
                    }else{
                      msgBox.error("Erro", data.msg);
                    }
                })
                .error(function (data){
                    msgBox.error("Erro", data.msg);
                });
          };

          //Função que adcionar um novo aparelho
          $scope.adicionar = function (){
                rest.post('aparelho', $scope.aparelho)
                  .success(function (data) {
                      if(data.status=="sucesso") {
                          msgBox.success("Adicionado com sucesso", "Aparelho adicionado com sucesso");

                          setTimeout(function(){
                            $scope.loadListar();
                            $scope.aparelho = {};
                          }, 2000);

                      }else{
                          msgBox.error("Erro ao Adicionar", data.msg);
                      }
                  })
                  .error( function (data) {
                      msgBox.error("Erro ao Adicionar", data.msg);
                  });
          }

          //Função que adcionar um novo aparelho
          $scope.atualizar = function (){
                rest.put('aparelho', $scope.aparelho)
                  .success(function (data) {
                      if(data.status=="sucesso") {
                          msgBox.success("Atualizado com sucesso", "Aparelho atualizado com sucesso");

                          setTimeout(function(){
                            $scope.loadListar();
                            $scope.aparelho = {};
                          }, 2000);

                      }else{
                          msgBox.error("Erro ao Atualizar", data.msg);
                      }
                  })
                  .error( function (data) {
                      msgBox.error("Erro ao Atualizar", data.msg);
                  });
          }

          $scope.selectModulo = function(){
              angular.forEach($scope.modulos_tomadas, function(value, key){
                  if(value.no_modulo == $scope.aparelho.no_modulo){
                    this.push(value.tomada);
                  }
              }, $scope.tomadas);

              if($scope.tomadas.length > 0){
                  $("#tomada").prop('disabled', false);
              }
          };
    };

    this.listar = function ($q, rest, msgBox){
        var deferred = $q.defer();
        rest.get('aparelho')
          .success(function (data) {
            if(data.status=="sucesso") {
              deferred.resolve(data.aparelhos);
            }else{
              deferred.reject(data);
              msgBox.error("Erro ao Adicionar", data.msg);
            }
          })
          .error( function (data) {
            msgBox.error("Erro", data.msg);
            deferred.reject(data);
          });
        return deferred.promise;
    };

    this.loadAnalise = function ($scope, rest, $q, breadcrumb, msgBox){
        $scope.total_dia = 0;
        $scope.total_ano = 0;
        $scope.total_mes = 0;
        $scope.total_analisado = 0;
        $scope.id_aparelho_analisado = 0;
        $scope.first_execucao = true;
        $scope.chart = {};
        $scope.aparelho = {};
        $scope.ativo = false;
        $scope.interval_aparelho_uso= {};
        $scope.interval_totals= {};

        $scope.reset = function () {
              $scope.total_dia = 0;
              $scope.total_ano = 0;
              $scope.total_mes = 0;
              $scope.total_analisado = 0;
              $scope.id_aparelho_analisado = 0;
              $scope.first_execucao = true;
              $scope.chart = {};
              $scope.aparelho = {};
              $scope.ativo = false;
              clearInterval($scope.interval_aparelho_uso);
              clearInterval($scope.interval_totals);
        };

        $scope.openAnalise = function(id_aparelho) {
              $scope.id_aparelho_analisado = id_aparelho;

              $scope.getAparelho(id_aparelho);
              $scope.func = 'analise';

        };

        $scope.iniciaAnalise = function () {
            $scope.getTotalAnalisado()
            $scope.getTotalDia();
            $scope.getTotalAno();
            $scope.getTotalMes();

           $scope.interval_totals =  setInterval(function(){
                $scope.getTotalDia();
                $scope.getTotalAno();
                $scope.getTotalMes();
                $scope.getTotalAnalisado();
            }, 10000);

        };

        $scope.getTotalDia = function () {
              rest.post('aparelho/total_dia',
              {
                  id_aparelho:$scope.id_aparelho_analisado,
                  data_atual: moment().format('YYYY-MM-DD')
              })
              .success(function (data) {
                  if(data.status=="sucesso") {
                      $scope.total_dia = data.total_dia;
                  }else{
                      msgBox.error("Erro", data.msg);
                  }
              })
              .error( function (data) {
                  msgBox.error("Erro", data.msg);
              });
        };

        $scope.getTotalAno = function () {
              rest.post('aparelho/total_ano',
              {
                  id_aparelho: $scope.id_aparelho_analisado,
                  data_atual: moment().format('YYYY-MM-DD')
              })
                .success(function (data) {
                    if(data.status=="sucesso") {
                        $scope.total_ano = data.total_ano;
                    }else{
                        msgBox.error("Erro", data.msg);
                    }
                })
                .error( function (data) {
                    msgBox.error("Erro", data.msg);
                });
        };

        $scope.getTotalMes = function () {
              rest.post('aparelho/total_mes',
              {
                  id_aparelho: $scope.id_aparelho_analisado,
                  mes_atual: moment().format('YYYY-MM')
              })
                .success(function (data) {
                    if(data.status=="sucesso") {
                        $scope.total_mes= data.total_mes;
                    }else{
                        msgBox.error("Erro", data.msg);
                    }
                })
                .error( function (data) {
                    msgBox.error("Erro", data.msg);
                });
        };

        $scope.getTotalAnalisado = function () {
              rest.post('aparelho/total_analisado',
              {
                  id_aparelho: $scope.id_aparelho_analisado,
              })
                .success(function (data) {
                    if(data.status=="sucesso") {
                        $scope.total_analisado= data.total_analisado;
                    }else{
                        msgBox.error("Erro", data.msg);
                    }
                })
                .error( function (data) {
                    msgBox.error("Erro", data.msg);
                });
        };

        $scope.getAparelhoAtivo = function (){
              var data = {};
              data. id_aparelho = $scope.id_aparelho_analisado;

              rest.post('aparelho/aparelho_ativo',data)
                .success(function (data) {
                    if(data.status=="sucesso") {
                        $scope.aparelho_ativo= data.kw_analisado;
                        if($scope.first_execucao){
                            $scope.preparaGraficoUso();
                            $scope.first_execucao = false;
                        }

                    }else{
                        msgBox.error("Erro", data.msg);
                    }
                })
                .error( function (data) {
                    msgBox.error("Erro", data.msg);
                });
        };

        $scope.getValues = function (series) {
            var data = {};
            data. id_aparelho = $scope.id_aparelho_analisado;
            data. id_modulo_tomada =  $scope.aparelho_ativo[$scope.aparelho_ativo.length -1].id_modulo_tomada;

            rest.post('aparelho/aparelho_ativo',data)
              .success(function (data) {
                  if(data.status=="sucesso") {
                      if(data.kw_analisado != 0){
                          $scope.aparelho_ativo= data.kw_analisado;

                          angular.forEach($scope.aparelho_ativo, function (value, i) {
                              var x = (new Date(value.data_hora)).getTime();
                              var y = parseFloat(value.corrente);
                              series.addPoint([x, y], true, true);
                          });
                      }

                  }else{
                      msgBox.error("Erro", data.msg);
                  }
              })
              .error( function (data) {
                  msgBox.error("Erro", data.msg);
              });
        }

        $scope.preparaGraficoUso = function (){
              $scope.chart.id_container = '#chart';

              var serie = [];

              var serie = {
                 type: 'line',
                 data: [],
                 name: $scope.aparelho.nome_aparelho
              };

              angular.forEach($scope.aparelho_ativo, function (value, i) {
                  serie.data.push([ (new Date(value.data_hora)).getTime(), parseFloat(value.corrente) ]);
              });

              // chart.addSeries(serie);
              $($scope.chart.id_container).highcharts({
                    chart: {
                        // height: 500,
                        zoomType: 'xy',
                        pinchType: 'true',
                        events: {
                            load: function () {
                                // set up the updating of the chart each second

                                if($scope.ativo){
                                  var series = this.series[0];
                                  $scope.interval_aparelho_uso = setInterval(function () {
                                      $scope.getValues(series);
                                  }, 5000);
                                }
                            }
                        }
                    },
                    exporting: {
                        enabled: false
                    },
                    xAxis: {
                        gridLineWidth: 1,
                        labels: {
                            formatter: function () {
                                return moment(this.value).format("DD/MM/YYYY")
                          }
                        }
                    },
                    yAxis: [{
                        title: {
                            text: 'Valores (Wh)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    }],
                    tooltip: {
                        crosshairs: true,
                        //shared: true
                        formatter: function () {
                            return '<b>' + this.series.name + '</b><br/>Valor: ' + this.y.toFixed(2) + '<br/>Data: ' + moment(parseInt(this.x)).format("DD/MM/YYYY HH:mm:ss")
                        }
                    },
                    title: {
                        text: ' ',
                        x: -20 //center
                    },
                    plotOptions: {
                        line: {
                            marker: {
                                enabled: false,
                            }
                        },
                        area: {
                            marker: {
                                enabled: false,
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [serie]
              });
        };
    };

};
