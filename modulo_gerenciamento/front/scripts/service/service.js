Application.factory('rest', function ($http){
  return {
    get: function (url) {
      return $http({
        method: 'GET',
        url: (API.server + url),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
      });
    },
    post: function (url, data) {
      return $http({
        method: 'POST',
        url: (API.server + url),
        data: $.param(data),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
      });
    },
    put: function (url, data) {
        return $http({
            method: 'PUT',
            url: (API.server + url),
            data: $.param(data),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
    },
    delete: function (url){
      return $http.delete(API.server + url);
    },
  }
});

Application.factory('breadcrumb', function ($rootScope) {

    return {

        data: {},

        set: function( data ){
            this.data = data;
            $rootScope.$broadcast('updateBreadcrumb');
        }
    }
});

Application.factory("msgBox", function () {

    return {

        error: function (title, text) {
          swal({
            title: title,
            text: text,
            type: "error",
            html: true,
            timer: 4000,
            showConfirmButton: true
          });


        },
        success: function (title, text) {
          swal({
            title: title,
            text: text,
            type: "success",
            timer: 2000,
            showConfirmButton: true
          });

        },
        functionDelete: function (fn){
            swal({
                title: "Excluir",
                text: "Tem certeza que deseja excluir o Aparelho?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                closeOnConfirm: false,
                closeButtonText: "Não",
                 },
                function(){
                 fn();
            });
        }
    };
});