/* ========================
 * CONFIG FOR SYSTEM
* @author FABRICIO
* @created 08/12/2014
* @version 1.0
* ========================*/

'use strict';

Application.config(function ($stateProvider, $urlRouterProvider) {

    //The default route
    $urlRouterProvider.otherwise("/aparelho");

    //Routing the states of application
    $stateProvider.state('dashboard', {
        url: "/",
        controller: ApplicationController.load,
    	templateUrl: API.templatePath + "/dashboard/dashboard.html"
    	//controller: DeviceController.load
    });
    $stateProvider.state('aparelho', {
        url: "/aparelho",
    	templateUrl: API.templatePath + "/aparelho/loadTemplate.html",
    	controller: AparelhoController.load,
        resolve: {
              //scripts: PidController.scripts,
              aparelhos: AparelhoController.listar
          }
    });

//    //Routing the states of application
//    $stateProvider
//    	.state('device', {
//    		url: "/cadastro/aparelho",
//    		templateUrl: API.templatePath + "/device/registration-device.html"
//    		//controller: DeviceController.load
//    	});
//
//    //Routing the states of application
//    $stateProvider
//        .state('arduino', {
//            url: "/cadastro/arduino",
//            templateUrl: API.templatePath + "/device/registration-arduino.html"
//           	//controller: RegistrationController.load
//        });
//
//	//Routing the states of application
//    $stateProvider
//        .state('list-device', {
//            url: "/listar/aparelho",
//            templateUrl: API.templatePath + "/device/list-device.html",
//           	controller: DeviceController.listDevice
//        });
//    //Routing the states of application
//    $stateProvider
//        .state('list-arduino', {
//            url: "/listar/arduino",
//            templateUrl: API.templatePath + "/device/list-arduino.html",
//           	controller: DeviceController.listArduino
//        });
});

// .state('filter', {
//             url: "/filter",
//             templateUrl: API.templatePath + "/filter.html",
//             controller: HomeController.init/*,
//             resolve:{
//                 promise: HomeController.promiseInit
//             }*/
//         });