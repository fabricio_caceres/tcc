/* ========================
 * API
* @author FABRICIO
* @created 08/12/2014 09:05:02
* @version 1.0
* ========================*/

'use strict';


var API = new function(){

    // CONTRUCTOR
    this.init = function (path)
    {
      this.templatePath = 'template/'; // VAR WHERE THE ARE TEMPLATES
      this.imageUrl = 'img/'; // VAR WHERE THE ARE IMG
      this.server = 'http://localhost/projeto_tcc/modulo_gerenciamento/back/';
      this.apiVersion = '';
    };

    this.localeDateRangePicker = function () {
        return {
            applyLabel: 'Aplicar',
            cancelLabel: 'Cancelar',
            fromLabel: 'De',
            toLabel: 'Até',
            customRangeLabel: 'Personalizado..',
            daysOfWeek: ['Dom', '2ª', '3ª', '4ª', '5ª', '6ª', 'Sáb'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'April', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            firstDay: 1,
            format: 'DD/MM/YYYY HH:MM:SS'
        };
    };

};