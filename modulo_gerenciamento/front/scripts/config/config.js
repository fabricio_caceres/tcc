/* ========================
 * CONFIG FOR SYSTEM
* @author FABRICIO
* @created 08/12/2014
* @version 1.0
* ========================*/

'use strict'

//START ANGULAR JS
var Application = angular.module('Application', ['ui.router', 'angular.filter'])
 	.run(function($rootScope) {

		$rootScope.$on('$stateChangeStart',
			function(event, toState, toParams, fromState, fromParams){
				$('.preloader').show();
			});

		$rootScope.$on('$stateChangeSuccess',
			function(event, toState, toParams, fromState, fromParams){
				$('.preloader').hide();
			});

    $rootScope.$safeApply = function ( $scope, fn ) {
        fn = fn || function () {};
        if ( $scope.$$phase ) {
            // não se preocupe, o valor é definido
            // e o Angular o pega...
            fn();
        }
        else {
            // isto vai disparar e dizer ao AngularJS que
            // uma mudança ocorreu se isso estiver
            // fora de seu próprio comportamento
            $scope.$apply( fn );
        }
    };


	});
