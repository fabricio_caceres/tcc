
-- -----------------------------------------------------
-- Table usuario
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS usuario (
  id_usuario SERIAL PRIMARY KEY,
  nome_usuario TEXT NOT NULL,
  celular TEXT NULL,
  email TEXT NOT NULL,
  senha TEXT NOT NULL,
  perfil INT NOT NULL DEFAULT 1,
  flg_ativo INT NOT NULL DEFAULT 1);


-- -----------------------------------------------------
-- Table inmetro
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS inmetro (
  id_inmetro SERIAL PRIMARY KEY,
  nome_fabricante TEXT NOT NULL,
  nome_marca TEXT NOT NULL,
  media REAL NULL,
  tensao REAL NULL,
  potencia REAL NULL,
  frequencia REAL NULL,
  classe VARCHAR(1) NULL,
  minimo REAL NULL,
  maximo REAL NULL,
  consumo TEXT NULL,
  id_usuario INT NOT NULL,
  nome_modelo TEXT NULL,
  CONSTRAINT fk_inmetro_1
    FOREIGN KEY (id_usuario)
    REFERENCES usuario (id_usuario)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);


-- -----------------------------------------------------
-- Table aparelho
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS aparelho (
  id_aparelho SERIAL PRIMARY KEY,
  nome_aparelho TEXT NOT NULL,
  modelo_aparelho TEXT NOT NULL,
  id_inmetro INT NULL,
  id_usuario INT NULL,
  localizacao text NULL,
  CONSTRAINT fk_aparelho_3
    FOREIGN KEY (id_inmetro)
    REFERENCES inmetro (id_inmetro)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT fk_aparelho_4
    FOREIGN KEY (id_usuario)
    REFERENCES usuario (id_usuario)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);



-- -----------------------------------------------------
-- Table modulo_fase
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS modulo_fase (
  id_modulo_fase SERIAL PRIMARY KEY,
  fase_1 REAL NULL,
  fase_2 REAL NULL,
  fase_3 REAL NULL,
  dthr TIMESTAMP NULL,
  tensao REAL NULL);


-- -----------------------------------------------------
-- Table modulo_tomada
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS modulo_tomada (
  id_modulo_tomada SERIAL PRIMARY KEY,
  data_hora TIMESTAMP NOT NULL,
  no_modulo TEXT NOT NULL,
  tomada TEXT NOT NULL,
  corrente REAL NULL );


-- -----------------------------------------------------
-- Table periodo_aparelho
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS periodo_aparelho (
  id_periodo_aparelho SERIAL PRIMARY KEY,
  tomada TEXT NULL,
  no_modulo TEXT NULL,
  id_aparelho INT NULL,
  dthr_inicio TIMESTAMP NULL,
  dthr_fim TIMESTAMP NULL,
  CONSTRAINT fk_periodo_aparelho_1
    FOREIGN KEY (id_aparelho)
    REFERENCES aparelho (id_aparelho)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);
