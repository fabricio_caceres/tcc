-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema sys_monitory
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema sys_monitory
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sys_monitory` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
-- -----------------------------------------------------
-- Schema teste_slim
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema teste_slim
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `teste_slim` DEFAULT CHARACTER SET latin1 ;
USE `sys_monitory` ;

-- -----------------------------------------------------
-- Table `sys_monitory`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sys_monitory`.`usuario` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nome_usuario` VARCHAR(45) NOT NULL COMMENT '',
  `celular` VARCHAR(45) NULL COMMENT '',
  `email` VARCHAR(45) NOT NULL COMMENT '',
  `senha` VARCHAR(45) NOT NULL COMMENT '',
  `perfil` INT(1) NOT NULL DEFAULT 1 COMMENT '',
  `flg_ativo` INT(1) NOT NULL DEFAULT 1 COMMENT '',
  PRIMARY KEY (`id_usuario`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sys_monitory`.`inmetro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sys_monitory`.`inmetro` (
  `id_inmetro` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nome_fabricante` VARCHAR(100) NOT NULL COMMENT '',
  `nome_marca` VARCHAR(100) NOT NULL COMMENT '',
  `media` FLOAT NULL COMMENT '',
  `tensao` FLOAT NULL COMMENT '',
  `potencia` FLOAT NULL COMMENT '',
  `frequencia` FLOAT NULL COMMENT '',
  `classe` VARCHAR(1) NULL COMMENT '',
  `minimo` FLOAT NULL COMMENT '',
  `maximo` FLOAT NULL COMMENT '',
  `consumo` VARCHAR(45) NULL COMMENT '',
  `id_usuario` INT NOT NULL COMMENT '',
  `nome_modelo` VARCHAR(100) NULL COMMENT '',
  PRIMARY KEY (`id_inmetro`)  COMMENT '',
  INDEX `fk_inmetro_1_idx` (`id_usuario` ASC)  COMMENT '',
  CONSTRAINT `fk_inmetro_1`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `sys_monitory`.`usuario` (`id_usuario`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sys_monitory`.`aparelho`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sys_monitory`.`aparelho` (
  `id_aparelho` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nome_aparelho` VARCHAR(45) NOT NULL COMMENT '',
  `id_inmetro` INT NULL COMMENT '',
  `id_modulo_tomada` INT NOT NULL COMMENT '',
  `id_usuario` INT NOT NULL COMMENT '',
  `id_tomada` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id_aparelho`)  COMMENT '',
  INDEX `fk_aparelho_3_idx` (`id_inmetro` ASC)  COMMENT '',
  INDEX `fk_aparelho_4_idx` (`id_usuario` ASC)  COMMENT '',
  CONSTRAINT `fk_aparelho_3`
    FOREIGN KEY (`id_inmetro`)
    REFERENCES `sys_monitory`.`inmetro` (`id_inmetro`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_aparelho_4`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `sys_monitory`.`usuario` (`id_usuario`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sys_monitory`.`modulo_fase`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sys_monitory`.`modulo_fase` (
  `id_modulo_fase` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `fase_1` DOUBLE NULL COMMENT '',
  `fase_2` DOUBLE NULL COMMENT '',
  `fase_3` DOUBLE NULL COMMENT '',
  `tensao` DOUBLE NULL COMMENT '',
  PRIMARY KEY (`id_modulo_fase`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sys_monitory`.`modulo_tomada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sys_monitory`.`modulo_tomada` (
  `id_modulo_tomada` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `data_hora` DATE NOT NULL COMMENT '',
  `no_modulo` VARCHAR(45) NOT NULL COMMENT '',
  `tomada` VARCHAR(45) NOT NULL COMMENT '',
  `corrente` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`id_modulo_tomada`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sys_monitory`.`periodo_aparelho`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sys_monitory`.`periodo_aparelho` (
  `id_periodo_aparelho` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `tomada` VARCHAR(45) NULL COMMENT '',
  `no_modulo` VARCHAR(45) NULL COMMENT '',
  `id_aparelho` INT NULL COMMENT '',
  `dthr_inicio` DATE NULL COMMENT '',
  `dthr_fim` DATE NULL COMMENT '',
  PRIMARY KEY (`id_periodo_aparelho`)  COMMENT '',
  INDEX `fk_periodo_aparelho_1_idx` (`id_aparelho` ASC)  COMMENT '',
  CONSTRAINT `fk_periodo_aparelho_1`
    FOREIGN KEY (`id_aparelho`)
    REFERENCES `sys_monitory`.`aparelho` (`id_aparelho`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_periodo_aparelho_2`
    FOREIGN KEY ()
    REFERENCES `sys_monitory`.`modulo_tomada` ()
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `teste_slim` ;

-- -----------------------------------------------------
-- Table `teste_slim`.`filme`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teste_slim`.`filme` (
  `id_filme` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `nome_filme` VARCHAR(255) NOT NULL COMMENT '',
  `diretor_filme` VARCHAR(255) NOT NULL COMMENT '',
  `ano_filme` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`id_filme`)  COMMENT '')
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

